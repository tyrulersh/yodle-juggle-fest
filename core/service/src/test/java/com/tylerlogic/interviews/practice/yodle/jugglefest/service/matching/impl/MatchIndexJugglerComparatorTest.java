package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl;

import java.util.Comparator;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

public class MatchIndexJugglerComparatorTest {
    @Rule
    public final JUnitRuleMockery context = new JUnitRuleMockery();

    private static final Juggler JUGGLER1 = new Juggler("J1", null, null);
    private static final Juggler JUGGLER2 = new Juggler("J2", null, null);

    @Mock
    private MatchIndexCalculator matchIndexCalculator;
    private Circuit circuit = new Circuit("placeholder", null);
    private Comparator<Juggler> comparator;

    @Before
    public void setupCut() {
        comparator = new MatchIndexJugglerComparator(matchIndexCalculator, circuit);
    }

    @Test
    public void compareJugglersWhenLeftLessThanRightDueToMatchIndex() {
        context.checking(new Expectations() {
            {
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER1);
                will(returnValue(1));
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER2);
                will(returnValue(2));
            }
        });
        assertThat(comparator.compare(JUGGLER1, JUGGLER2), is(lessThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftGreaterThanRightDueToMatchIndex() {
        context.checking(new Expectations() {
            {
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER1);
                will(returnValue(2));
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER2);
                will(returnValue(1));
            }
        });
        assertThat(comparator.compare(JUGGLER1, JUGGLER2), is(greaterThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftLessThanRightDueToName() {
        context.checking(new Expectations() {
            {
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER1);
                will(returnValue(1));
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER2);
                will(returnValue(1));
            }
        });
        assertThat(comparator.compare(JUGGLER1, JUGGLER2), is(lessThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftGreaterThanRightDueToName() {
        context.checking(new Expectations() {
            {
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER1);
                will(returnValue(1));
                oneOf(matchIndexCalculator).calculate(circuit, JUGGLER2);
                will(returnValue(1));
            }
        });
        assertThat(comparator.compare(JUGGLER2, JUGGLER1), is(greaterThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftEqualToRight() {
        final Juggler anotherJuggler1 = new Juggler("J1", null, null);
        context.checking(new Expectations() {
            {
                exactly(2).of(matchIndexCalculator).calculate(circuit, JUGGLER1);
                will(returnValue(1));
                exactly(2).of(matchIndexCalculator).calculate(circuit, anotherJuggler1);
                will(returnValue(1));
            }
        });
        assertThat(comparator.compare(JUGGLER1, anotherJuggler1), is(0));
        assertThat(comparator.compare(anotherJuggler1, JUGGLER1), is(0));
    }
}
