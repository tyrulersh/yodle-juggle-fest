package com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.impl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.JugglerPrinter;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.RosterPrinter;

import static com.google.common.collect.Sets.newLinkedHashSet;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RosterPrinterTest {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    @Mock
    private JugglerPrinter jugglerPrinter;
    private RosterPrinter printer;

    @Before
    public void setupCut() {
        printer = new RosterPrinterImpl(jugglerPrinter);
    }

    @Test
    public void print() {
        final Circuit circuit1 = new Circuit("C1", null);
        final Circuit circuit2 = new Circuit("C2", null);
        final Circuit circuit3 = new Circuit("C3", null);
        final Juggler juggler1 = new Juggler("J1", null, null);
        final Juggler juggler2 = new Juggler("J2", null, null);
        final Juggler juggler3 = new Juggler("J3", null, null);
        final Juggler juggler4 = new Juggler("J4", null, null);
        final Juggler juggler5 = new Juggler("J5", null, null);
        final Juggler juggler6 = new Juggler("J6", null, null);
        final Juggler juggler7 = new Juggler("J7", null, null);
        final Juggler juggler8 = new Juggler("J8", null, null);
        final Juggler juggler9 = new Juggler("J9", null, null);
        context.checking(new Expectations() {
            {
                oneOf(jugglerPrinter).print(juggler1);
                will(returnValue("printedJuggler1"));
                oneOf(jugglerPrinter).print(juggler2);
                will(returnValue("printedJuggler2"));
                oneOf(jugglerPrinter).print(juggler3);
                will(returnValue("printedJuggler3"));
                oneOf(jugglerPrinter).print(juggler4);
                will(returnValue("printedJuggler4"));
                oneOf(jugglerPrinter).print(juggler5);
                will(returnValue("printedJuggler5"));
                oneOf(jugglerPrinter).print(juggler6);
                will(returnValue("printedJuggler6"));
                oneOf(jugglerPrinter).print(juggler7);
                will(returnValue("printedJuggler7"));
                oneOf(jugglerPrinter).print(juggler8);
                will(returnValue("printedJuggler8"));
                oneOf(jugglerPrinter).print(juggler9);
                will(returnValue("printedJuggler9"));
            }
        });
        Map<Circuit, Set<Juggler>> assignments = new LinkedHashMap<>();
        assignments.put(circuit1, newLinkedHashSet(asList(juggler1, juggler2, juggler3)));
        assignments.put(circuit2, newLinkedHashSet(asList(juggler4, juggler5, juggler6)));
        assignments.put(circuit3, newLinkedHashSet(asList(juggler7, juggler8, juggler9)));
        Roster roster = new Roster(assignments);
        assertThat(printer.print(roster), is(equalTo(
                "C1 printedJuggler1, printedJuggler2, printedJuggler3" + LINE_SEPARATOR +
                "C2 printedJuggler4, printedJuggler5, printedJuggler6" + LINE_SEPARATOR +
                "C3 printedJuggler7, printedJuggler8, printedJuggler9")));
    }

    @Test
    public void printingNullReturnsEmptyString() {
        assertThat(printer.print(null), is(equalTo("")));
    }
}
