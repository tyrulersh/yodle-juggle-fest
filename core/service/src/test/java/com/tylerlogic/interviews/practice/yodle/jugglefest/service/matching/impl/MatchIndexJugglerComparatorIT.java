package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl;

import java.util.Comparator;

import javax.annotation.Resource;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.application.ServiceApplicationContext;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceApplicationContext.class)
public class MatchIndexJugglerComparatorIT {
    private static final Juggler LOW_SKILL_JUGGLER = new Juggler("J1", new SkillSet(1, 1, 1), null);
    private static final Juggler HIGH_SKILL_JUGGLER =
            new Juggler("J2", new SkillSet(2, 2, 2), null);
    private static final Circuit CIRCUIT = new Circuit("circuit", new SkillSet(1, 1, 1));

    @Resource
    private MatchIndexCalculator matchIndexCalculator;
    private Comparator<Juggler> comparator;

    @Before
    public void setupCut() {
        comparator = new MatchIndexJugglerComparator(matchIndexCalculator, CIRCUIT);
    }

    @Test
    public void compareJugglersWhenLeftLessThanRightDueToMatchIndex() {
        assertThat(comparator.compare(LOW_SKILL_JUGGLER, HIGH_SKILL_JUGGLER), is(lessThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftGreaterThanRightDueToMatchIndex() {
        assertThat(comparator.compare(HIGH_SKILL_JUGGLER, LOW_SKILL_JUGGLER), is(greaterThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftLessThanRightDueToName() {
        Juggler lowSkillJugglerWithGreaterName = new Juggler(LOW_SKILL_JUGGLER.getName() +
                "extra", LOW_SKILL_JUGGLER.getSkillSet(), null);
        assertThat(comparator.compare(LOW_SKILL_JUGGLER, lowSkillJugglerWithGreaterName),
                is(lessThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftGreaterThanRightDueToName() {
        Juggler lowSkillJugglerWithGreaterName = new Juggler(LOW_SKILL_JUGGLER.getName() +
                "extra", LOW_SKILL_JUGGLER.getSkillSet(), null);
        assertThat(comparator.compare(lowSkillJugglerWithGreaterName, LOW_SKILL_JUGGLER),
                is(greaterThan(0)));
    }

    @Test
    public void compareJugglersWhenLeftEqualToRight() {
        assertThat(comparator.compare(LOW_SKILL_JUGGLER, LOW_SKILL_JUGGLER), is(0));
    }
}
