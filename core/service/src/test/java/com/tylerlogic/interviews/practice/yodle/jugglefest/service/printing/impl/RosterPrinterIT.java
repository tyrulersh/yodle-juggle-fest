package com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.impl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;

import javax.annotation.Resource;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.application.ServiceApplicationContext;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.RosterPrinter;

import static com.google.common.collect.Sets.newLinkedHashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceApplicationContext.class)
public class RosterPrinterIT {
    @Resource
    private RosterPrinter printer;

    @Test
    public void print() {
        Circuit circuit1 = new Circuit("C1", new SkillSet(1, 0, 0));
        Circuit circuit2 = new Circuit("C2", new SkillSet(2, 0, 0));
        Circuit circuit3 = new Circuit("C3", new SkillSet(3, 0, 0));
        Juggler juggler1 = new Juggler("J1", new SkillSet(1, 0, 0), asList(circuit1));
        Juggler juggler2 = new Juggler("J2", new SkillSet(2, 0, 0), asList(circuit2));
        Juggler juggler3 = new Juggler("J3", new SkillSet(3, 0, 0), asList(circuit3));
        Juggler juggler4 = new Juggler("J4", new SkillSet(4, 0, 0), asList(circuit1, circuit2));
        Juggler juggler5 = new Juggler("J5", new SkillSet(5, 0, 0), asList(circuit2, circuit1));
        Juggler juggler6 = new Juggler("J6", new SkillSet(6, 0, 0), asList(circuit1, circuit3));
        Juggler juggler7 = new Juggler("J7", new SkillSet(7, 0, 0), asList(circuit3, circuit1));
        Juggler juggler8 = new Juggler("J8", new SkillSet(8, 0, 0), asList(circuit2, circuit3));
        Juggler juggler9 = new Juggler("J9", new SkillSet(9, 0, 0), asList(circuit3, circuit2));
        Map<Circuit, Set<Juggler>> assignments = new LinkedHashMap<>();
        assignments.put(circuit1, newLinkedHashSet(asList(juggler1, juggler2, juggler3)));
        assignments.put(circuit2, newLinkedHashSet(asList(juggler4, juggler5, juggler6)));
        assignments.put(circuit3, newLinkedHashSet(asList(juggler7, juggler8, juggler9)));
        Roster roster = new Roster(assignments);
        assertThat(printer.print(roster), is(equalTo(
                "C1 J1 C1:1, J2 C2:4, J3 C3:9" + LINE_SEPARATOR +
                "C2 J4 C1:4 C2:8, J5 C2:10 C1:5, J6 C1:6 C3:18" + LINE_SEPARATOR +
                "C3 J7 C3:21 C1:7, J8 C2:16 C3:24, J9 C3:27 C2:18")));
    }
}
