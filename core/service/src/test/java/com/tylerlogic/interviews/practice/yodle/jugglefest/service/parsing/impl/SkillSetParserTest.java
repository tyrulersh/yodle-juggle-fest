package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.SkillSetParser;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class SkillSetParserTest {
    private SkillSetParser parser = new SkillSetParserImpl();

    @Test
    public void parse() {
        assertThat(parser.parse("H:123 E:456 P:789"), is(equalTo(new SkillSet(123, 456, 789))));
    }

    @Test
    public void parseReturnsNullForNull() {
        assertThat(parser.parse(null), is(nullValue()));
    }

    @Test
    public void parseReturnsNullForEmptyString() {
        assertThat(parser.parse(""), is(nullValue()));
    }

    @Test
    public void parseReturnsNullWhenSkillSetIsNotInCorrectFormat() {
        assertThat(parser.parse("doesnt conform"), is(nullValue()));
    }
}
