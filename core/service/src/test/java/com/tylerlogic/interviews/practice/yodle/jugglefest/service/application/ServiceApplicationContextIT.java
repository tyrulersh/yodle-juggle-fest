package com.tylerlogic.interviews.practice.yodle.jugglefest.service.application;

import javax.annotation.Resource;

import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl.MatchIndexCalculatorImpl;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.CircuitParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JuggleFestParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JugglerParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.CircuitParserImpl;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.JuggleFestParserImpl;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.JugglerParserImpl;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterService;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.impl.RosterServiceImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceApplicationContext.class)
public class ServiceApplicationContextIT {
    @Resource
    private JugglerParser jugglerParser;
    @Resource
    private CircuitParser circuitParser;
    @Resource
    private JuggleFestParser juggleFestParser;
    @Resource
    private MatchIndexCalculator matchIndexCalculator;
    @Resource
    private RosterService rosterService;

    @Test
    public void jugglerParserExists() {
        assertThat(jugglerParser, is(notNullValue()));
        assertThat(jugglerParser, is(instanceOf(JugglerParserImpl.class)));
    }

    @Test
    public void circuitParserExists() {
        assertThat(circuitParser, is(notNullValue()));
        assertThat(circuitParser, is(instanceOf(CircuitParserImpl.class)));
    }

    @Test
    public void juggleFestParserExists() {
        assertThat(juggleFestParser, is(instanceOf(JuggleFestParserImpl.class)));
    }

    @Test
    public void matchIndexCalculatorExists() {
        assertThat(matchIndexCalculator, is(instanceOf(MatchIndexCalculatorImpl.class)));
    }

    @Test
    public void rosterServiceExists() {
        assertThat(rosterService, is(instanceOf(RosterServiceImpl.class)));
    }
}
