package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import static java.util.Arrays.asList;

import com.tylerlogic.interviews.practice.yodle.jugglefest.exceptions.NonexistentPreferredCircuitException;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JugglerParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.SkillSetParser;

import static com.google.common.collect.Sets.newHashSet;

import org.apache.commons.lang3.StringUtils;
import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class JugglerParserTest {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    private static final Circuit CIRCUIT1 = new Circuit("C1", new SkillSet(1, 2, 3));
    private static final Circuit CIRCUIT2 = new Circuit("C2", new SkillSet(4, 5, 6));
    private static final Circuit CIRCUIT3 = new Circuit("C3", new SkillSet(7, 8, 9));
    private static final Map<String, Circuit> CIRCUIT_NAME_TO_CIRCUIT_MAP;

    static {
        CIRCUIT_NAME_TO_CIRCUIT_MAP = new HashMap<String, Circuit>();
        CIRCUIT_NAME_TO_CIRCUIT_MAP.put("C1", CIRCUIT1);
        CIRCUIT_NAME_TO_CIRCUIT_MAP.put("C2", CIRCUIT2);
        CIRCUIT_NAME_TO_CIRCUIT_MAP.put("C3", CIRCUIT3);
    }

    private static final SkillSet SKILL_SET1 = new SkillSet(123, 456, 789);
    private static final SkillSet SKILL_SET2 = new SkillSet(456, 789, 123);
    private static final SkillSet SKILL_SET3 = new SkillSet(789, 123, 456);
    private static final Juggler JUGGLER1 = new Juggler("J1", SKILL_SET1, asList(CIRCUIT1));
    private static final Juggler JUGGLER2 =
            new Juggler("J2", SKILL_SET2, asList(CIRCUIT2, CIRCUIT3));
    private static final Juggler JUGGLER3 =
            new Juggler("J3", SKILL_SET3, asList(CIRCUIT1, CIRCUIT2, CIRCUIT3));

    private static final String SKILL_SET1_DESCRIPTION = "H:123 E:456 P:789";
    private static final String SKILL_SET2_DESCRIPTION = "H:456 E:789 P:123";
    private static final String SKILL_SET3_DESCRIPTION = "H:789 E:123 P:456";
    private static final String JUGGLER1_DESCRIPTION = "J J1 " + SKILL_SET1_DESCRIPTION + " C1";
    private static final String JUGGLER2_DESCRIPTION = "J J2 " + SKILL_SET2_DESCRIPTION + " C2,C3";
    private static final String JUGGLER3_DESCRIPTION =
            "J J3 " + SKILL_SET3_DESCRIPTION + " C1,C2,C3";

    @Mock
    private SkillSetParser skillSetParser;
    private JugglerParser parser;

    @Before
    public void setupCut() {
        parser = new JugglerParserImpl(skillSetParser);
    }

    @Test
    public void parsingDescriptionOfSingleJuggler() {
        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
            }
        });
        Set<Juggler> jugglers =
                parser.parse(new Scanner(JUGGLER1_DESCRIPTION), CIRCUIT_NAME_TO_CIRCUIT_MAP);
        assertThat(jugglers, is(equalTo(asSet(JUGGLER1))));
    }

    @Test
    public void parsingDescriptionOfMultipleJugglers() {
        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
                oneOf(skillSetParser).parse(SKILL_SET2_DESCRIPTION);
                will(returnValue(SKILL_SET2));
                oneOf(skillSetParser).parse(SKILL_SET3_DESCRIPTION);
                will(returnValue(SKILL_SET3));
            }
        });
        String description =
                joinAsLines(JUGGLER1_DESCRIPTION, JUGGLER2_DESCRIPTION, JUGGLER3_DESCRIPTION);
        Set<Juggler> jugglers =
                parser.parse(new Scanner(description), CIRCUIT_NAME_TO_CIRCUIT_MAP);
        assertThat(jugglers, is(equalTo(asSet(JUGGLER1, JUGGLER2, JUGGLER3))));
    }

    @Test
    public void parsingEmptyJugglerDescriptionReturnsNoJugglers() {
        Set<Juggler> jugglers = parser.parse(new Scanner(""), CIRCUIT_NAME_TO_CIRCUIT_MAP);
        assertThat(jugglers, is(empty()));
    }

    @Test
    public void parsingTextWithNoJugglerDescriptionsReturnsNoJugglers() {
        String description = joinAsLines("First non-juggler text", "Second non-juggler text");
        Set<Juggler> jugglers =
                parser.parse(new Scanner(description), CIRCUIT_NAME_TO_CIRCUIT_MAP);
        assertThat(jugglers, is(empty()));
    }

    @Test
    public void parsingJugglersDescriptionPrefixedWithNonJugglerTextReturnsNoJugglers() {
        String description = joinAsLines("Non-juggler text", JUGGLER1_DESCRIPTION,
                JUGGLER2_DESCRIPTION, JUGGLER3_DESCRIPTION);
        Set<Juggler> jugglers =
                parser.parse(new Scanner(description), CIRCUIT_NAME_TO_CIRCUIT_MAP);
        assertThat(jugglers, is(empty()));
    }

    @Test
    public void parsingJugglersDescriptionSuffixedWithNonJugglerTextReturnsAllLeadingJugglers() {
        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
                oneOf(skillSetParser).parse(SKILL_SET2_DESCRIPTION);
                will(returnValue(SKILL_SET2));
                oneOf(skillSetParser).parse(SKILL_SET3_DESCRIPTION);
                will(returnValue(SKILL_SET3));
            }
        });
        String description =
                joinAsLines(JUGGLER1_DESCRIPTION, JUGGLER2_DESCRIPTION, JUGGLER3_DESCRIPTION,
                        "Non-juggler text", "J OtherJuggler H:1 E:2 P:3 C1");
        Set<Juggler> jugglers =
                parser.parse(new Scanner(description), CIRCUIT_NAME_TO_CIRCUIT_MAP);
        assertThat(jugglers, is(equalTo(asSet(JUGGLER1, JUGGLER2, JUGGLER3))));
    }

    @Test
    public void parsingJugglerPreferringNonexistentCircuitCausesNonexistentPrefCircuitException() {
        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
            }
        });
        try {
            String jugglerDescription = "J juggler " + SKILL_SET1_DESCRIPTION + " circuit";
            parser.parse(new Scanner(jugglerDescription), Collections.emptyMap());
        } catch (NonexistentPreferredCircuitException exception) {
            assertThat(exception.getMessage(), is(equalTo(
                    "Juggler 'juggler' prefers nonexistent circuit: 'circuit'")));
        }
    }

    private <T> Set<T> asSet(@SuppressWarnings("unchecked") T... elements) {
        return newHashSet(elements);
    }

    private String joinAsLines(String... elements) {
        return StringUtils.join(elements, LINE_SEPARATOR);
    }
}
