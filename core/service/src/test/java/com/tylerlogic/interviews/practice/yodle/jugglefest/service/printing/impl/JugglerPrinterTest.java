package com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.impl;

import static java.util.Arrays.asList;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.JugglerPrinter;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class JugglerPrinterTest {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    @Mock
    private MatchIndexCalculator matchIndexCalculator;
    private JugglerPrinter printer;

    @Before
    public void setupCut() {
        printer = new JugglerPrinterImpl(matchIndexCalculator);
    }

    @Test
    public void print() {
        final Circuit circuit1 = new Circuit("C1", null);
        final Circuit circuit2 = new Circuit("C2", null);
        final Circuit circuit3 = new Circuit("C3", null);
        final Juggler juggler = new Juggler("J1", null, asList(circuit1, circuit2, circuit3));
        context.checking(new Expectations() {
            {
                oneOf(matchIndexCalculator).calculate(circuit1, juggler);
                will(returnValue(123));
                oneOf(matchIndexCalculator).calculate(circuit2, juggler);
                will(returnValue(456));
                oneOf(matchIndexCalculator).calculate(circuit3, juggler);
                will(returnValue(789));
            }
        });
        assertThat(printer.print(juggler), is(equalTo("J1 C1:123 C2:456 C3:789")));
    }

    @Test
    public void printingNullReturnsEmptyString() {
        assertThat(printer.print(null), is(equalTo("")));
    }
}
