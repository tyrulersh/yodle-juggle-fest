package com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.asList;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexJugglerComparatorFactory;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterServiceHelper;

import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newLinkedHashSet;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class RosterServiceHelperTest {
    @Rule
    public final JUnitRuleMockery context = new JUnitRuleMockery();

    private static final Circuit CIRCUIT1 = new Circuit("C1", null);
    private static final Circuit CIRCUIT2 = new Circuit("C2", null);
    private static final Circuit CIRCUIT3 = new Circuit("C3", null);

    // This comparator compares based on the int in the test jugglers names, simply for ease of
    // testing.
    private static final Comparator<Juggler> NAME_NUMBER_COMPARATOR = (Juggler j1, Juggler j2)
            -> parseInt(j1.getName().substring(1)) - parseInt(j2.getName().substring(1));

    @Mock
    private MatchIndexJugglerComparatorFactory matchIndexJugglerComparatorFactory;
    private RosterServiceHelper helper;

    @Before
    public void setupCut() {
        helper = new RosterServiceHelperImpl(matchIndexJugglerComparatorFactory);
    }

    @Test
    public void createEmptyCircuitToAssignmentsMap() {
        final Comparator<Juggler> comparator1 = (Juggler j1, Juggler j2) -> 0;
        final Comparator<Juggler> comparator2 = (Juggler j1, Juggler j2) -> 0;
        context.checking(new Expectations() {
            {
                oneOf(matchIndexJugglerComparatorFactory).create(CIRCUIT1);
                will(returnValue(comparator1));
                oneOf(matchIndexJugglerComparatorFactory).create(CIRCUIT2);
                will(returnValue(comparator2));
            }
        });
        Set<Circuit> circuits = newHashSet(CIRCUIT1, CIRCUIT2);
        Map<Circuit, SortedSet<Juggler>> assignmentsMap =
                helper.createEmptyCircuitToAssignmentsMap(circuits);
        assertThat(assignmentsMap.keySet(), is(equalTo(circuits)));
        assertThat(assignmentsMap.get(CIRCUIT1), is(empty()));
        assertThat(assignmentsMap.get(CIRCUIT1).comparator(), is(sameInstance(comparator1)));
        assertThat(assignmentsMap.get(CIRCUIT2), is(empty()));
        assertThat(assignmentsMap.get(CIRCUIT2).comparator(), is(sameInstance(comparator2)));
    }

    @Test
    public void getFirstPreferredCircuitIndexWhenStartingCircuitHasCapacity() {
        Juggler juggler = new Juggler("J1", null, asList(CIRCUIT1, CIRCUIT2));
        Map<Circuit, SortedSet<Juggler>> assignmentsMap = new HashMap<>();
        assignmentsMap.put(CIRCUIT2, new TreeSet<>(NAME_NUMBER_COMPARATOR));
        int circuitIndex = helper.getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                juggler, 1, assignmentsMap, 5);
        assertThat(circuitIndex, is(1));
    }

    @Test
    public void getFirstPrefCircuitIndexWhenStartingCircuitHasNoCapacityAndInputJugglerIsWorse() {
        Juggler juggler1 = new Juggler("J1", null, asList(CIRCUIT1, CIRCUIT2));
        Juggler juggler2 = new Juggler("J2", null, null);
        Juggler juggler3 = new Juggler("J3", null, null);

        Map<Circuit, SortedSet<Juggler>> assignmentsMap = new HashMap<>();
        assignmentsMap.put(CIRCUIT1, newTreeSet(NAME_NUMBER_COMPARATOR, juggler2, juggler3));
        assignmentsMap.put(CIRCUIT2, new TreeSet<>(NAME_NUMBER_COMPARATOR));

        int circuitIndex = helper.getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                juggler1, 0, assignmentsMap, 2);
        assertThat(circuitIndex, is(1));
    }

    @Test
    public void getFirstPreferredCircuitIndexWhenManyCircuitsHaveNoCapacityAndInputJugglerIsWorse()
    {
        Circuit circuit4 = new Circuit("C4", null);
        // a fourth preferred circuit ensure testing that's distinct from test with similar use
        // case: getFirstPreferredCircuitWhenNoPreferredCircuitsHaveCapacityForJuggler
        Juggler juggler1 = new Juggler("J1", null, asList(CIRCUIT1, CIRCUIT2, CIRCUIT3, circuit4));
        Juggler juggler2 = new Juggler("J2", null, null);
        Juggler juggler3 = new Juggler("J3", null, null);

        Map<Circuit, SortedSet<Juggler>> assignmentsMap = new HashMap<>();
        assignmentsMap.put(CIRCUIT1, newTreeSet(NAME_NUMBER_COMPARATOR, juggler2));
        assignmentsMap.put(CIRCUIT2, newTreeSet(NAME_NUMBER_COMPARATOR, juggler3));
        assignmentsMap.put(CIRCUIT3, new TreeSet<>(NAME_NUMBER_COMPARATOR));
        assignmentsMap.put(circuit4, new TreeSet<>(NAME_NUMBER_COMPARATOR));

        int circuitIndex = helper.getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                juggler1, 0, assignmentsMap, 1);
        assertThat(circuitIndex, is(2));
    }

    @Test
    public void getFirstPrefCircuitIndexWhenStartingCircuitHasNoCapacityAndInputJugglerIsBetter() {
        Juggler juggler1 = new Juggler("J1", null, null);
        Juggler juggler2 = new Juggler("J2", null, null);
        Juggler juggler3 = new Juggler("J3", null, asList(CIRCUIT1, CIRCUIT2));

        Map<Circuit, SortedSet<Juggler>> assignmentsMap = new HashMap<>();
        assignmentsMap.put(CIRCUIT1, newTreeSet(NAME_NUMBER_COMPARATOR, juggler1, juggler2));
        assignmentsMap.put(CIRCUIT2, new TreeSet<>(NAME_NUMBER_COMPARATOR));

        int circuitIndex = helper.getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                juggler3, 0, assignmentsMap, 2);
        assertThat(circuitIndex, is(0));
    }

    @Test
    public void getFirstPreferredCircuitIndexWhenNoPreferredCircuitsHaveCapacityForJuggler() {
        Juggler juggler1 = new Juggler("J1", null, asList(CIRCUIT1, CIRCUIT2));
        Juggler juggler2 = new Juggler("J2", null, null);
        Juggler juggler3 = new Juggler("J3", null, null);

        Map<Circuit, SortedSet<Juggler>> assignmentsMap = new HashMap<>();
        assignmentsMap.put(CIRCUIT1, newTreeSet(NAME_NUMBER_COMPARATOR, juggler2));
        assignmentsMap.put(CIRCUIT2, newTreeSet(NAME_NUMBER_COMPARATOR, juggler3));

        int circuitIndex = helper.getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                juggler1, 0, assignmentsMap, 1);
        assertThat(circuitIndex, is(2));
    }

    @Test
    public void assignResidualJugglers() {
        Juggler juggler1 = new Juggler("J1", null, null);
        Juggler juggler2 = new Juggler("J2", null, null);
        Juggler juggler3 = new Juggler("J3", null, null);
        Juggler juggler4 = new Juggler("J4", null, null);
        Juggler juggler5 = new Juggler("J5", null, null);
        Juggler juggler6 = new Juggler("J6", null, null);

        // using linked hash map allows for deterministic iteration which is good for the
        // correctness of assertions in this test
        Map<Circuit, SortedSet<Juggler>> assignmentsMap = new LinkedHashMap<>();
        // full circuit
        assignmentsMap.put(CIRCUIT1, new TreeSet<>(NAME_NUMBER_COMPARATOR));
        assignmentsMap.get(CIRCUIT1).add(juggler1);
        assignmentsMap.get(CIRCUIT1).add(juggler2);
        // partially full circuit
        assignmentsMap.put(CIRCUIT2, new TreeSet<>(NAME_NUMBER_COMPARATOR));
        assignmentsMap.get(CIRCUIT2).add(juggler3);
        // empty circuit
        assignmentsMap.put(CIRCUIT3, new TreeSet<>(NAME_NUMBER_COMPARATOR));
        helper.assignResidualJugglers(
                newLinkedHashSet(asList(juggler4, juggler5, juggler6)), assignmentsMap, 2);
        assertThat(assignmentsMap.keySet(), is(equalTo(newHashSet(CIRCUIT1, CIRCUIT2, CIRCUIT3))));
        assertThat(assignmentsMap.get(CIRCUIT1), is(equalTo(newHashSet(juggler1, juggler2))));
        assertThat(assignmentsMap.get(CIRCUIT2), is(equalTo(newHashSet(juggler3, juggler4))));
        assertThat(assignmentsMap.get(CIRCUIT3), is(equalTo(newHashSet(juggler5, juggler6))));
    }

    @Test
    public void assignResidualJugglersForEmptyResidualJugglerSet() {
        Juggler juggler1 = new Juggler("J1", null, asList(CIRCUIT1));
        Map<Circuit, SortedSet<Juggler>> assignmentsMap = new HashMap<>();
        assignmentsMap.put(CIRCUIT1, new TreeSet<>(NAME_NUMBER_COMPARATOR));
        assignmentsMap.get(CIRCUIT1).add(juggler1);
        helper.assignResidualJugglers(Collections.emptySet(), assignmentsMap, 1);
        assertThat(assignmentsMap.keySet(), is(equalTo(newHashSet(CIRCUIT1))));
        assertThat(assignmentsMap.get(CIRCUIT1), is(equalTo(newHashSet(juggler1))));
    }

    @SuppressWarnings("unchecked")
    private <T> TreeSet<T> newTreeSet(Comparator<T> comparator, T... ts) {
        TreeSet<T> set = new TreeSet<>(comparator);
        for (T t : ts) {
            set.add(t);
        }
        return set;
    }
}
