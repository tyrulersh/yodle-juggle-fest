package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.util.Arrays.asList;

import javax.annotation.Resource;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.application.ServiceApplicationContext;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JuggleFestParser;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLE_FEST;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceApplicationContext.class)
public class JuggleFestParserIT {
    private static final String JUGGLE_FEST_DESCRIPTION_WITH_ENDING_NEWLINE_CLASSPATH =
            "/JuggleFestParserIT/juggle-fest-description-with-ending-newline.txt";
    private static final String JUGGLE_FEST_DESCRIPTION_WITHOUT_ENDING_NEWLINE_CLASSPATH =
            "/JuggleFestParserIT/juggle-fest-description-without-ending-newline.txt";
    private static final String JUGGLE_FEST_DESCRIPTION_WITH_LOTS_OF_CIRCUITS_CLASSPATH =
            "/JuggleFestParserIT/juggle-fest-description-with-lots-of-circuits.txt";

    @Resource
    private JuggleFestParser parser;

    @Test
    public void parseDescriptionWithEndingNewline() {
        InputStream description = JuggleFestParserIT.class.getResourceAsStream(
                JUGGLE_FEST_DESCRIPTION_WITH_ENDING_NEWLINE_CLASSPATH);
        assertThat("Unable to find on classpath: " +
                JUGGLE_FEST_DESCRIPTION_WITH_ENDING_NEWLINE_CLASSPATH, description,
                is(notNullValue()));
        JuggleFest juggleFest = parser.parse(new Scanner(description));
        assertThat(juggleFest, is(equalTo(JUGGLE_FEST)));
    }

    @Test
    public void parseDescriptionWithoutEndingNewline() {
        InputStream description = JuggleFestParserIT.class.getResourceAsStream(
                JUGGLE_FEST_DESCRIPTION_WITHOUT_ENDING_NEWLINE_CLASSPATH);
        assertThat("Unable to find on classpath: " +
                JUGGLE_FEST_DESCRIPTION_WITHOUT_ENDING_NEWLINE_CLASSPATH, description,
                is(notNullValue()));
        JuggleFest juggleFest = parser.parse(new Scanner(description));
        assertThat(juggleFest, is(equalTo(JUGGLE_FEST)));
    }

    @Test
    public void circuitOrderingIsPreserved() {
        InputStream description = JuggleFestParserIT.class.getResourceAsStream(
                JUGGLE_FEST_DESCRIPTION_WITH_LOTS_OF_CIRCUITS_CLASSPATH);
        assertThat("Unable to find on classpath: " +
                JUGGLE_FEST_DESCRIPTION_WITH_LOTS_OF_CIRCUITS_CLASSPATH, description,
                is(notNullValue()));
        List<String> circuitNamesInIterationOrder =
                getCircuitNamesInIterationOrder(parser.parse(new Scanner(description)));
        assertThat(circuitNamesInIterationOrder,
                is(equalTo(asList("C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9"))));
    }

    private List<String> getCircuitNamesInIterationOrder(JuggleFest juggleFest) {
        List<String> list = new ArrayList<>();
        for (Circuit circuit : juggleFest.getCircuits()) {
            list.add(circuit.getName());
        }
        return list;
    }
}
