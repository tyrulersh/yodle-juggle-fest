package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import static java.util.Arrays.asList;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;

import static com.google.common.collect.Sets.newHashSet;

/**
 * Constants pertaining to the example provided in the JuggleFest's problem description:
 * http://www.yodlecareers.com/puzzles/jugglefest.html
 */
public class ExampleJuggleFestTestConstants {
    public static final Circuit CIRCUIT0 = new Circuit("C0", new SkillSet(7, 7, 10));
    public static final Circuit CIRCUIT1 = new Circuit("C1", new SkillSet(2, 1, 1));
    public static final Circuit CIRCUIT2 = new Circuit("C2", new SkillSet(7, 6, 4));

    public static final Juggler JUGGLER0 =
            new Juggler("J0", new SkillSet(3, 9, 2), asList(CIRCUIT2, CIRCUIT0, CIRCUIT1));
    public static final Juggler JUGGLER1 =
            new Juggler("J1", new SkillSet(4, 3, 7), asList(CIRCUIT0, CIRCUIT2, CIRCUIT1));
    public static final Juggler JUGGLER2 =
            new Juggler("J2", new SkillSet(4, 0, 10), asList(CIRCUIT0, CIRCUIT2, CIRCUIT1));
    public static final Juggler JUGGLER3 =
            new Juggler("J3", new SkillSet(10, 3, 8), asList(CIRCUIT2, CIRCUIT0, CIRCUIT1));
    public static final Juggler JUGGLER4 =
            new Juggler("J4", new SkillSet(6, 10, 1), asList(CIRCUIT0, CIRCUIT2, CIRCUIT1));
    public static final Juggler JUGGLER5 =
            new Juggler("J5", new SkillSet(6, 7, 7), asList(CIRCUIT0, CIRCUIT2, CIRCUIT1));
    public static final Juggler JUGGLER6 =
            new Juggler("J6", new SkillSet(8, 6, 9), asList(CIRCUIT2, CIRCUIT1, CIRCUIT0));
    public static final Juggler JUGGLER7 =
            new Juggler("J7", new SkillSet(7, 1, 5), asList(CIRCUIT2, CIRCUIT1, CIRCUIT0));
    public static final Juggler JUGGLER8 =
            new Juggler("J8", new SkillSet(8, 2, 3), asList(CIRCUIT1, CIRCUIT0, CIRCUIT2));
    public static final Juggler JUGGLER9 =
            new Juggler("J9", new SkillSet(10, 2, 1), asList(CIRCUIT1, CIRCUIT2, CIRCUIT0));
    public static final Juggler JUGGLER10 =
            new Juggler("J10", new SkillSet(6, 4, 5), asList(CIRCUIT0, CIRCUIT2, CIRCUIT1));
    public static final Juggler JUGGLER11 =
            new Juggler("J11", new SkillSet(8, 4, 7), asList(CIRCUIT0, CIRCUIT1, CIRCUIT2));

    public static final JuggleFest JUGGLE_FEST = new JuggleFest(
            newHashSet(CIRCUIT0, CIRCUIT1, CIRCUIT2),
            newHashSet(JUGGLER0, JUGGLER1, JUGGLER2, JUGGLER3, JUGGLER4, JUGGLER5, JUGGLER6,
                    JUGGLER7, JUGGLER8, JUGGLER9, JUGGLER10, JUGGLER11));

    private ExampleJuggleFestTestConstants() {
    }
}
