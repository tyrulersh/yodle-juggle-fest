package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class MatchIndexCalculatorTest {
    private MatchIndexCalculator calculator = new MatchIndexCalculatorImpl();

    @Test
    public void handEyeCoordinationProduct() {
        Circuit circuit = new Circuit("c", new SkillSet(3, 0, 0));
        Juggler juggler = new Juggler("j", new SkillSet(5, 0, 0), null);
        assertThat(calculator.calculate(circuit, juggler), is(15));
    }

    @Test
    public void enduranceProduct() {
        Circuit circuit = new Circuit("c", new SkillSet(0, 3, 0));
        Juggler juggler = new Juggler("j", new SkillSet(0, 5, 0), null);
        assertThat(calculator.calculate(circuit, juggler), is(15));
    }

    @Test
    public void pizazzProduct() {
        Circuit circuit = new Circuit("c", new SkillSet(0, 0, 3));
        Juggler juggler = new Juggler("j", new SkillSet(0, 0, 5), null);
        assertThat(calculator.calculate(circuit, juggler), is(15));
    }

    @Test
    public void completeCalculation() {
        SkillSet skillSetOne = new SkillSet(3, 5, 7);
        SkillSet skillSetTwo = new SkillSet(11, 13, 17);

        Circuit circuit = new Circuit("c", skillSetOne);
        Juggler juggler = new Juggler("j", skillSetTwo, null);
        assertThat(calculator.calculate(circuit, juggler), is(217));

        circuit = new Circuit("c", skillSetTwo);
        juggler = new Juggler("j", skillSetOne, null);
        assertThat(calculator.calculate(circuit, juggler), is(217));
    }
}
