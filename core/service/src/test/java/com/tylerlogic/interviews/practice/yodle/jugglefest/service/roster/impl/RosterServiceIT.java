package com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.impl;

import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;

import javax.annotation.Resource;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.application.ServiceApplicationContext;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterService;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.CIRCUIT0;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.CIRCUIT1;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.CIRCUIT2;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER0;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER1;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER10;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER11;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER2;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER3;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER4;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER5;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER6;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER7;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER8;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLER9;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl.ExampleJuggleFestTestConstants.JUGGLE_FEST;

import static com.google.common.collect.Sets.newHashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceApplicationContext.class)
public class RosterServiceIT {
    private static final SkillSet ONES_SKILL_SET = new SkillSet(1, 1, 1);
    private static final SkillSet TWOS_SKILL_SET = new SkillSet(2, 2, 2);

    @Resource
    private RosterService service;

    @Test
    public void jugglersAreAssignedToTheirPreferredCircuits() {
        Circuit circuit1 = new Circuit("C1", ONES_SKILL_SET);
        Circuit circuit2 = new Circuit("C2", ONES_SKILL_SET);
        Set<Circuit> circuits = newHashSet(circuit1, circuit2);

        Juggler juggler1 = new Juggler("J1", ONES_SKILL_SET, asList(circuit1));
        Juggler juggler2 = new Juggler("J2", ONES_SKILL_SET, asList(circuit2));
        JuggleFest juggleFest = new JuggleFest(circuits, newHashSet(juggler1, juggler2));
        Map<Circuit, Set<Juggler>> assignments =
                service.assignJugglersToCircuits(juggleFest).getAssignments();
        assertThat(assignments.get(circuit1), is(equalTo(newHashSet(juggler1))));
        assertThat(assignments.get(circuit2), is(equalTo(newHashSet(juggler2))));

        // preferences here are opposite of above
        juggler1 = new Juggler("J1", ONES_SKILL_SET, asList(circuit2));
        juggler2 = new Juggler("J2", ONES_SKILL_SET, asList(circuit1));
        juggleFest = new JuggleFest(circuits, newHashSet(juggler1, juggler2));
        assignments = service.assignJugglersToCircuits(juggleFest).getAssignments();
        assertThat(assignments.get(circuit1), is(equalTo(newHashSet(juggler2))));
        assertThat(assignments.get(circuit2), is(equalTo(newHashSet(juggler1))));
    }

    @Test
    public void jugglersAreAssignedToThePreferredCircuitTheyMatchBest() {
        Circuit circuit1 = new Circuit("C1", ONES_SKILL_SET);
        Circuit circuit2 = new Circuit("C2", ONES_SKILL_SET);
        Set<Circuit> circuits = newHashSet(circuit1, circuit2);

        // juggler2 "wins" circuit1
        Juggler juggler1 = new Juggler("J1", ONES_SKILL_SET, asList(circuit1));
        Juggler juggler2 = new Juggler("J2", TWOS_SKILL_SET, asList(circuit1));
        JuggleFest juggleFest = new JuggleFest(circuits, newHashSet(juggler1, juggler2));
        Map<Circuit, Set<Juggler>> assignments =
                service.assignJugglersToCircuits(juggleFest).getAssignments();
        assertThat(assignments.get(circuit1), is(equalTo(newHashSet(juggler2))));
        assertThat(assignments.get(circuit2), is(equalTo(newHashSet(juggler1))));

        // juggler2 "wins" circuit2
        juggler1 = new Juggler("J1", ONES_SKILL_SET, asList(circuit2));
        juggler2 = new Juggler("J2", TWOS_SKILL_SET, asList(circuit2));
        juggleFest = new JuggleFest(circuits, newHashSet(juggler1, juggler2));
        assignments = service.assignJugglersToCircuits(juggleFest).getAssignments();
        assertThat(assignments.get(circuit1), is(equalTo(newHashSet(juggler1))));
        assertThat(assignments.get(circuit2), is(equalTo(newHashSet(juggler2))));
    }

    @Test
    public void assignJugglersToCircuits() {
        Map<Circuit, Set<Juggler>> assignments =
                service.assignJugglersToCircuits(JUGGLE_FEST).getAssignments();
        assertThat(assignments.get(CIRCUIT0),
                is(equalTo(newHashSet(JUGGLER5, JUGGLER11, JUGGLER2, JUGGLER4))));
        assertThat(assignments.get(CIRCUIT1),
                is(equalTo(newHashSet(JUGGLER9, JUGGLER8, JUGGLER7, JUGGLER1))));
        assertThat(assignments.get(CIRCUIT2),
                is(equalTo(newHashSet(JUGGLER6, JUGGLER3, JUGGLER10, JUGGLER0))));
    }
}
