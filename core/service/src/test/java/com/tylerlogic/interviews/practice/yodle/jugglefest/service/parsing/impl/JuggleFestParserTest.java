package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.CircuitParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JuggleFestParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JugglerParser;

import static com.google.common.collect.Sets.newHashSet;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class JuggleFestParserTest {
    @Rule
    public final JUnitRuleMockery context = new JUnitRuleMockery();

    private static final Circuit CIRCUIT1 = new Circuit("C1", null);
    private static final Circuit CIRCUIT2 = new Circuit("C2", null);
    private static final Circuit CIRCUIT3 = new Circuit("C3", null);
    private static final Map<String, Circuit> CIRCUIT_NAME_TO_CIRCUIT_MAP;

    static {
        CIRCUIT_NAME_TO_CIRCUIT_MAP = new HashMap<String, Circuit>();
        CIRCUIT_NAME_TO_CIRCUIT_MAP.put("C1", CIRCUIT1);
        CIRCUIT_NAME_TO_CIRCUIT_MAP.put("C2", CIRCUIT2);
        CIRCUIT_NAME_TO_CIRCUIT_MAP.put("C3", CIRCUIT3);
    }

    private static final Juggler JUGGLER1 = new Juggler("J1", null, null);
    private static final Juggler JUGGLER2 = new Juggler("J2", null, null);
    private static final Juggler JUGGLER3 = new Juggler("J3", null, null);

    @Mock
    private CircuitParser circuitParser;
    @Mock
    private JugglerParser jugglerParser;
    private JuggleFestParser parser;

    @Before
    public void setupCut() {
        parser = new JuggleFestParserImpl(circuitParser, jugglerParser);
    }

    @Test
    public void parse() {
        final Scanner juggleFestDescription = new Scanner("this is a place-holder");
        final Set<Circuit> parsedCircuits = newHashSet(CIRCUIT1, CIRCUIT2, CIRCUIT3);
        final Set<Juggler> parsedJugglers = newHashSet(JUGGLER1, JUGGLER2, JUGGLER3);
        context.checking(new Expectations() {
            {
                oneOf(circuitParser).parse(juggleFestDescription);
                will(returnValue(parsedCircuits));
                oneOf(jugglerParser).parse(juggleFestDescription, CIRCUIT_NAME_TO_CIRCUIT_MAP);
                will(returnValue(parsedJugglers));
            }
        });
        JuggleFest juggleFest = parser.parse(juggleFestDescription);
        assertThat(juggleFest, is(equalTo(new JuggleFest(parsedCircuits, parsedJugglers))));
    }
}
