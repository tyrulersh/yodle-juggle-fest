package com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.asList;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterService;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterServiceHelper;

import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newLinkedHashSet;

import org.jmock.Expectations;
import org.jmock.Sequence;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Due to the complexity of the CUT, unit testing is not straight forward, so some of the code
 * coverage for the CUT is shared with the integration test, {@link
 * com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.impl.RosterServiceIT
 * RosterServiceIT}
 */
public class RosterServiceTest {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    private static final Circuit CIRCUIT1 = new Circuit("C1", null);
    private static final Circuit CIRCUIT2 = new Circuit("C2", null);
    private static final Set<Circuit> CIRCUITS = newHashSet(CIRCUIT1, CIRCUIT2);

    // This comparator compares based on the int in the test jugglers names, simply for ease of
    // testing.
    private static final Comparator<Juggler> NAME_NUMBER_COMPARATOR = (Juggler j1, Juggler j2)
            -> parseInt(j1.getName().substring(1)) - parseInt(j2.getName().substring(1));

    @Mock
    private RosterServiceHelper helper;
    private RosterService service;

    @Before
    public void setupCut() {
        service = new RosterServiceImpl(helper);
    }

    @Test
    public void assignJugglersToCircuits() {
        Juggler juggler1 = new Juggler("J1", null, asList(CIRCUIT1));
        Juggler juggler2 = new Juggler("J2", null, asList(CIRCUIT2));

        Map<Circuit, SortedSet<Juggler>> assignments = new HashMap<>();
        CIRCUITS.stream().forEach((Circuit circuit) ->
                assignments.put(circuit, new TreeSet<>(NAME_NUMBER_COMPARATOR)));
        Set<Juggler> jugglers = newLinkedHashSet(asList(juggler1, juggler2));

        context.checking(new Expectations() {
            {
                oneOf(helper).createEmptyCircuitToAssignmentsMap(CIRCUITS);
                will(returnValue(assignments));
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler1, 0, assignments, 1);
                will(returnValue(0));
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler2, 0, assignments, 1);
                will(returnValue(0));
            }
        });
        JuggleFest juggleFest = new JuggleFest(CIRCUITS, jugglers);
        Roster roster = service.assignJugglersToCircuits(juggleFest);
        assertThat(roster, is(equalTo(new Roster(new HashMap<>(assignments)))));
    }

    @Test
    public void assignJugglersToCircuitsWhenJugglerSupplantingHappensInCircuitAssignments() {
        Juggler juggler1 = new Juggler("J1", null, asList(CIRCUIT1, CIRCUIT2));
        Juggler juggler2 = new Juggler("J2", null, asList(CIRCUIT1, CIRCUIT2));

        Map<Circuit, SortedSet<Juggler>> assignments = new HashMap<>();
        CIRCUITS.stream().forEach((Circuit circuit) ->
                assignments.put(circuit, new TreeSet<>(NAME_NUMBER_COMPARATOR)));
        Set<Juggler> jugglers = newLinkedHashSet(asList(juggler1, juggler2));

        Sequence jugglerOneIsSupplanted = context.sequence("jugglerOneIsSupplanted");
        context.checking(new Expectations() {
            {
                oneOf(helper).createEmptyCircuitToAssignmentsMap(CIRCUITS);
                will(returnValue(assignments));
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler1, 0, assignments, 1);
                will(returnValue(0));
                inSequence(jugglerOneIsSupplanted);
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler2, 0, assignments, 1);
                will(returnValue(0));
                inSequence(jugglerOneIsSupplanted);
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler1, 1, assignments, 1);
                will(returnValue(1));
                inSequence(jugglerOneIsSupplanted);
            }
        });
        JuggleFest juggleFest = new JuggleFest(CIRCUITS, jugglers);
        Roster roster = service.assignJugglersToCircuits(juggleFest);
        assertThat(roster, is(equalTo(new Roster(new HashMap<>(assignments)))));
    }

    @Test
    public void assignJugglersToCircuitsWithResidualJugglers() {
        Juggler juggler1 = new Juggler("J1", null, asList(CIRCUIT1));
        Juggler juggler2 = new Juggler("J2", null, asList(CIRCUIT1));
        Juggler juggler3 = new Juggler("J3", null, asList(CIRCUIT2));
        Juggler juggler4 = new Juggler("J4", null, asList(CIRCUIT2));

        Map<Circuit, SortedSet<Juggler>> assignments = new HashMap<>();
        Set<Juggler> jugglers = newLinkedHashSet(asList(juggler1, juggler2, juggler3, juggler4));

        context.checking(new Expectations() {
            {
                oneOf(helper).createEmptyCircuitToAssignmentsMap(CIRCUITS);
                will(returnValue(assignments));
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler1, 0, assignments, 2);
                will(returnValue(1));
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler2, 0, assignments, 2);
                will(returnValue(1));
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler3, 0, assignments, 2);
                will(returnValue(1));
                oneOf(helper).getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(
                        juggler4, 0, assignments, 2);
                will(returnValue(1));
                oneOf(helper).assignResidualJugglers(jugglers, assignments, 2);
            }
        });
        JuggleFest juggleFest = new JuggleFest(CIRCUITS, jugglers);
        Roster roster = service.assignJugglersToCircuits(juggleFest);
        assertThat(roster, is(equalTo(new Roster(new HashMap<>(assignments)))));
    }
}
