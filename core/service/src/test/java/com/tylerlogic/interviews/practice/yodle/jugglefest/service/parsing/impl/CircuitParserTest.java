package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import static java.util.Arrays.asList;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.CircuitParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.SkillSetParser;

import static com.google.common.collect.Sets.newHashSet;

import org.apache.commons.lang3.StringUtils;
import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CircuitParserTest {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    private static final SkillSet SKILL_SET1 = new SkillSet(123, 456, 789);
    private static final SkillSet SKILL_SET2 = new SkillSet(456, 789, 123);
    private static final SkillSet SKILL_SET3 = new SkillSet(789, 123, 456);
    private static final Circuit CIRCUIT1 = new Circuit("C1", SKILL_SET1);
    private static final Circuit CIRCUIT2 = new Circuit("C2", SKILL_SET2);
    private static final Circuit CIRCUIT3 = new Circuit("C3", SKILL_SET3);

    private static final String SKILL_SET1_DESCRIPTION = "H:123 E:456 P:789";
    private static final String SKILL_SET2_DESCRIPTION = "H:456 E:789 P:123";
    private static final String SKILL_SET3_DESCRIPTION = "H:789 E:123 P:456";
    private static final String CIRCUIT1_DESCRIPTION = "C C1 " + SKILL_SET1_DESCRIPTION;
    private static final String CIRCUIT2_DESCRIPTION = "C C2 " + SKILL_SET2_DESCRIPTION;
    private static final String CIRCUIT3_DESCRIPTION = "C C3 " + SKILL_SET3_DESCRIPTION;

    @Mock
    private SkillSetParser skillSetParser;
    private CircuitParser parser;

    @Before
    public void setupCut() {
        parser = new CircuitParserImpl(skillSetParser);
    }

    @Test
    public void parsingDescriptionOfSingleCircuit() {
        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
            }
        });
        Set<Circuit> circuits = parser.parse(new Scanner(CIRCUIT1_DESCRIPTION));
        assertThat(circuits, is(equalTo(asSet(CIRCUIT1))));
    }

    @Test
    public void parsingDescriptionOfMultipleCircuits() {
        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
                oneOf(skillSetParser).parse(SKILL_SET2_DESCRIPTION);
                will(returnValue(SKILL_SET2));
                oneOf(skillSetParser).parse(SKILL_SET3_DESCRIPTION);
                will(returnValue(SKILL_SET3));
            }
        });
        String description =
                joinAsLines(CIRCUIT1_DESCRIPTION, CIRCUIT2_DESCRIPTION, CIRCUIT3_DESCRIPTION);
        Set<Circuit> circuits = parser.parse(new Scanner(description));
        assertThat(circuits, is(equalTo(asSet(CIRCUIT1, CIRCUIT2, CIRCUIT3))));
    }

    @Test
    public void parsingEmptyCircuitDescriptionReturnsNoCircuits() {
        Set<Circuit> circuits = parser.parse(new Scanner(""));
        assertThat(circuits, is(empty()));
    }

    @Test
    public void parsingTextWithNoCircuitDescriptionsReturnsNoCircuits() {
        String description = joinAsLines("First non-circuit text", "Second non-circuit text");
        Set<Circuit> circuits = parser.parse(new Scanner(description));
        assertThat(circuits, is(empty()));
    }

    @Test
    public void parsingCircuitDescriptionPrefixedWithNonCircuitTextReturnsNoCircuits() {
        String description = joinAsLines("Non-circuit text", CIRCUIT1_DESCRIPTION,
                CIRCUIT2_DESCRIPTION, CIRCUIT3_DESCRIPTION);
        Set<Circuit> circuits = parser.parse(new Scanner(description));
        assertThat(circuits, is(empty()));
    }

    @Test
    public void parsingCircuitDescriptionSuffixedWithNonCircuitTextReturnsAllLeadingCircuits() {
        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
                oneOf(skillSetParser).parse(SKILL_SET2_DESCRIPTION);
                will(returnValue(SKILL_SET2));
                oneOf(skillSetParser).parse(SKILL_SET3_DESCRIPTION);
                will(returnValue(SKILL_SET3));
            }
        });
        String description =
                joinAsLines(CIRCUIT1_DESCRIPTION, CIRCUIT2_DESCRIPTION, CIRCUIT3_DESCRIPTION,
                        "Non-circuit text", "C OtherCircuit H:1 E:2 P:3");
        Set<Circuit> circuits = parser.parse(new Scanner(description));
        assertThat(circuits, is(equalTo(asSet(CIRCUIT1, CIRCUIT2, CIRCUIT3))));
    }

    @Test
    public void circuitOrderingIsPreserved() {
        SkillSet skillSet4 = new SkillSet(12, 34, 56);
        SkillSet skillSet5 = new SkillSet(78, 12, 34);
        Circuit circuit4 = new Circuit("C4", skillSet4);
        Circuit circuit5 = new Circuit("C5", skillSet5);

        String skillSet4Description = "H:12 E:34 P:56";
        String skillSet5Description = "H:78 E:12 P:34";
        String circuit4Description = "C C4 " + skillSet4Description;
        String circuit5Description = "C C5 " + skillSet5Description;

        context.checking(new Expectations() {
            {
                oneOf(skillSetParser).parse(SKILL_SET1_DESCRIPTION);
                will(returnValue(SKILL_SET1));
                oneOf(skillSetParser).parse(SKILL_SET2_DESCRIPTION);
                will(returnValue(SKILL_SET2));
                oneOf(skillSetParser).parse(SKILL_SET3_DESCRIPTION);
                will(returnValue(SKILL_SET3));
                oneOf(skillSetParser).parse(skillSet4Description);
                will(returnValue(skillSet4));
                oneOf(skillSetParser).parse(skillSet5Description);
                will(returnValue(skillSet5));
            }
        });

        String description = joinAsLines(CIRCUIT1_DESCRIPTION, CIRCUIT2_DESCRIPTION,
                CIRCUIT3_DESCRIPTION, circuit4Description, circuit5Description);
        List<Circuit> circuitInIterationOrder =
                new ArrayList<>(parser.parse(new Scanner(description)));
        assertThat(circuitInIterationOrder,
                is(equalTo(asList(CIRCUIT1, CIRCUIT2, CIRCUIT3, circuit4, circuit5))));
    }

    private <T> Set<T> asSet(@SuppressWarnings("unchecked") T... elements) {
        return newHashSet(elements);
    }

    private String joinAsLines(String... elements) {
        return StringUtils.join(elements, LINE_SEPARATOR);
    }
}
