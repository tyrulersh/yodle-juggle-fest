package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl;

import java.util.Comparator;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexJugglerComparatorFactory;

import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class MatchIndexJugglerComparatorFactoryTest {
    @Rule
    public final JUnitRuleMockery context = new JUnitRuleMockery();

    @Mock
    private MatchIndexCalculator matchIndexCalculator;
    private MatchIndexJugglerComparatorFactory factory;

    @Before
    public void setupCut() {
        factory = new MatchIndexJugglerComparatorFactoryImpl(matchIndexCalculator);
    }

    @Test
    public void create() {
        Circuit circuit = new Circuit(null, null);
        Comparator<Juggler> comparator = factory.create(circuit);
        Circuit circuitFromComparator =
                (Circuit)ReflectionTestUtils.getField(comparator, "circuit");
        assertThat(circuitFromComparator, is(sameInstance(circuit)));
        MatchIndexCalculator matchIndexCalculatorFromComparator =
                (MatchIndexCalculator)ReflectionTestUtils.getField(comparator,
                        "matchIndexCalculator");
        assertThat(matchIndexCalculatorFromComparator, is(sameInstance(matchIndexCalculator)));
    }
}
