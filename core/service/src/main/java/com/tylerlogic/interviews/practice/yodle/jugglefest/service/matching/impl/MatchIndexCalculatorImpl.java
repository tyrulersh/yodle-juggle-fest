package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl;

import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Named
public class MatchIndexCalculatorImpl implements MatchIndexCalculator {
    private static final Log LOG = LogFactory.getLog(MatchIndexCalculatorImpl.class);

    public int calculate(Circuit circuit, Juggler juggler) {
        SkillSet circuitSkillSet = circuit.getSkillSet();
        SkillSet jugglerSkillSet = juggler.getSkillSet();
        int index = dotProduct(circuitSkillSet, jugglerSkillSet);
        if (LOG.isTraceEnabled()) {
            LOG.trace(circuit + " and " + juggler + " match index: " + index);
        }
        return index;
    }

    private int dotProduct(SkillSet left, SkillSet right) {
        int product = left.getHandEyeCoordination() * right.getHandEyeCoordination();
        product += left.getEndurance() * right.getEndurance();
        product += left.getPizazz() * right.getPizazz();
        return product;
    }
}
