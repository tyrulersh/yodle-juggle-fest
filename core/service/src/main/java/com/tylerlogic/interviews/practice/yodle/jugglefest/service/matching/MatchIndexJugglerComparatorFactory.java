package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching;

import java.util.Comparator;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;

public interface MatchIndexJugglerComparatorFactory {
    Comparator<Juggler> create(Circuit circuit);
}
