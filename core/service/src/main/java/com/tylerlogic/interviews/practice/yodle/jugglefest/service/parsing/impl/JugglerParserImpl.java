package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.exceptions.NonexistentPreferredCircuitException;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JugglerParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.SkillSetParser;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.ParsingConstants.JUGGLER_DESCRIPTION_PATTERN;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;

@Named
public class JugglerParserImpl implements JugglerParser {
    private static final Log LOG = LogFactory.getLog(JugglerParserImpl.class);

    private final SkillSetParser skillSetParser;

    @Inject
    public JugglerParserImpl(SkillSetParser skillSetParser) {
        this.skillSetParser = skillSetParser;
    }

    public Set<Juggler> parse(Scanner descriptionScanner,
            Map<String, Circuit> circuitNameToCircuitMap) {
        Set<Juggler> jugglers = new HashSet<Juggler>();
        descriptionScanner.useDelimiter(LINE_SEPARATOR);
        while (descriptionScanner.hasNext(JUGGLER_DESCRIPTION_PATTERN)) {
            String jugglerDescription = descriptionScanner.next(JUGGLER_DESCRIPTION_PATTERN);
            Juggler juggler = parseJuggler(jugglerDescription, circuitNameToCircuitMap);
            jugglers.add(juggler);
            if (LOG.isTraceEnabled()) {
                LOG.trace("Parsed " + juggler + " from " + jugglerDescription);
            }
        }
        return jugglers;
    }

    private Juggler parseJuggler(String jugglerDescription,
            Map<String, Circuit> circuitNameToCircuitMap) {
        Matcher matcher = JUGGLER_DESCRIPTION_PATTERN.matcher(jugglerDescription);
        matcher.matches();
        String name = matcher.group(1);
        String skillSetDescription = matcher.group(2);
        // use the last group here because skillset uses groups.
        String preferredCircuitsCsvList = matcher.group(matcher.groupCount());
        SkillSet skillSet = skillSetParser.parse(skillSetDescription);
        List<Circuit> preferredCircuits =
                parsePreferredCircuits(name, preferredCircuitsCsvList, circuitNameToCircuitMap);
        return new Juggler(name, skillSet, preferredCircuits);
    }

    private List<Circuit> parsePreferredCircuits(String jugglerName,
            String preferredCircuitsCsvList, Map<String, Circuit> circuitNameToCircuitMap) {
        List<Circuit> circuits = new ArrayList<Circuit>();
        String[] circuitNames = preferredCircuitsCsvList.split(",");
        for (String name : circuitNames) {
            Circuit circuit = circuitNameToCircuitMap.get(name);
            if (circuit == null) {
                throw new NonexistentPreferredCircuitException(jugglerName, name);
            }
            circuits.add(circuit);
        }
        return circuits;
    }
}
