package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;

public interface MatchIndexCalculator {
    int calculate(Circuit circuit, Juggler juggler);
}
