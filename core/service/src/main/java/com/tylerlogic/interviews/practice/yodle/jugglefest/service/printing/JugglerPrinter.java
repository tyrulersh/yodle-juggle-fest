package com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;

public interface JugglerPrinter {
    String print(Juggler juggler);
}
