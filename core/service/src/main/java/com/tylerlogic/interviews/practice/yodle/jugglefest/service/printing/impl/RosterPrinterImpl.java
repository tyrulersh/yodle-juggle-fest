package com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.impl;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.JugglerPrinter;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.RosterPrinter;

import org.apache.commons.lang3.text.StrBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;

@Named
public class RosterPrinterImpl implements RosterPrinter {
    private static final Log LOG = LogFactory.getLog(RosterPrinterImpl.class);

    private static final char CIRCUIT_JUGGLERS_SEPARATOR = ' ';
    private static final String JUGGLERS_SEPARATOR = ", ";

    private final JugglerPrinter jugglerPrinter;

    @Inject
    public RosterPrinterImpl(JugglerPrinter jugglerPrinter) {
        this.jugglerPrinter = jugglerPrinter;
    }

    public String print(Roster roster) {
        StrBuilder builder = new StrBuilder();
        for (Circuit circuit : getAssignmentCircuits(roster)) {
            builder.appendSeparator(LINE_SEPARATOR);
            String name = circuit.getName();
            builder.append(name);
            builder.append(CIRCUIT_JUGGLERS_SEPARATOR);
            Map<Circuit, Set<Juggler>> assignments = roster.getAssignments();
            Set<Juggler> jugglers = assignments.get(circuit);
            appendJugglersDescription(builder, jugglers);
        }
        String value = builder.toString();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Printed " + roster + " as " + value);
        }
        return value;
    }

    private Set<Circuit> getAssignmentCircuits(Roster roster) {
        Set<Circuit> circuits = Collections.emptySet();
        if (roster != null) {
            Map<Circuit, Set<Juggler>> assignments = roster.getAssignments();
            circuits = assignments.keySet();
        }
        return circuits;
    }

    private void appendJugglersDescription(StrBuilder builder, Set<Juggler> jugglers) {
        boolean first = true;
        for (Juggler juggler : jugglers) {
            if (!first) {
                builder.append(JUGGLERS_SEPARATOR);
            }
            String jugglerDescription = jugglerPrinter.print(juggler);
            builder.append(jugglerDescription);
            first = false;
        }
    }
}
