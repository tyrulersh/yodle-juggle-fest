package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.CircuitParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.SkillSetParser;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.ParsingConstants.SKILLSET_DESCRIPTION_PATTERN;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;

@Named
public class CircuitParserImpl implements CircuitParser {
    private static final Log LOG = LogFactory.getLog(CircuitParserImpl.class);

    private static final Pattern CIRCUIT_DESCRIPTION_PATTERN =
            Pattern.compile("C (\\S+) (" + SKILLSET_DESCRIPTION_PATTERN + ")");

    private final SkillSetParser skillSetParser;

    @Inject
    public CircuitParserImpl(SkillSetParser skillSetParser) {
        this.skillSetParser = skillSetParser;
    }

    public Set<Circuit> parse(Scanner descriptionScanner) {
        descriptionScanner.useDelimiter(LINE_SEPARATOR);
        // use LinkedHashSet to preserve input order
        Set<Circuit> circuits = new LinkedHashSet<>();
        while (descriptionScanner.hasNext(CIRCUIT_DESCRIPTION_PATTERN)) {
            String circuitDescription = descriptionScanner.next(CIRCUIT_DESCRIPTION_PATTERN);
            Circuit circuit = parseCircuit(circuitDescription);
            circuits.add(circuit);
            if (LOG.isTraceEnabled()) {
                LOG.trace("Parsed " + circuit + " from " + circuitDescription);
            }
        }
        return circuits;
    }

    private Circuit parseCircuit(String description) {
        Matcher matcher = CIRCUIT_DESCRIPTION_PATTERN.matcher(description);
        matcher.matches();
        String name = matcher.group(1);
        String skillSetText = matcher.group(2);
        SkillSet skillSet = skillSetParser.parse(skillSetText);
        Circuit circuit = new Circuit(name, skillSet);
        return circuit;
    }
}
