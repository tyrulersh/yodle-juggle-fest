package com.tylerlogic.interviews.practice.yodle.jugglefest.service;

import org.springframework.context.annotation.ComponentScan;

/**
 * A marker interface for use by component scanning. Specifically the 'basePackageClasses' attribute
 * of spring's {@link ComponentScan} annotation.
 */
public interface ServicePackage {
}
