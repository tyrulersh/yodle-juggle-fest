package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.util.regex.Matcher;

import static java.lang.Integer.parseInt;

import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.SkillSetParser;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.ParsingConstants.SKILLSET_DESCRIPTION_PATTERN;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Named
public class SkillSetParserImpl implements SkillSetParser {
    private static final Log LOG = LogFactory.getLog(SkillSetParserImpl.class);

    public SkillSet parse(String description) {
        SkillSet skillSet = null;
        if (description != null) {
            Matcher matcher = SKILLSET_DESCRIPTION_PATTERN.matcher(description);
            if (matcher.matches()) {
                String handEyeCoordinationText = matcher.group(1);
                int handEyeCoordination = parseInt(handEyeCoordinationText);
                String enduranceText = matcher.group(2);
                int endurance = parseInt(enduranceText);
                String pizazzText = matcher.group(3);
                int pizazz = parseInt(pizazzText);
                skillSet = new SkillSet(handEyeCoordination, endurance, pizazz);
            }
            if (LOG.isTraceEnabled()) {
                LOG.trace("Parsed " + skillSet + " from " + description);
            }
        }
        return skillSet;
    }
}
