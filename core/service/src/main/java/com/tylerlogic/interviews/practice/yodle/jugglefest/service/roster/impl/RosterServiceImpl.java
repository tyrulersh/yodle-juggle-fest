package com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.impl;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterService;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterServiceHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Named
public class RosterServiceImpl implements RosterService {
    private static final Log LOG = LogFactory.getLog(RosterServiceImpl.class);

    private final RosterServiceHelper helper;

    @Inject
    public RosterServiceImpl(RosterServiceHelper helper) {
        this.helper = helper;
    }

    public Roster assignJugglersToCircuits(JuggleFest juggleFest) {
        Set<Circuit> circuits = juggleFest.getCircuits();
        Map<Circuit, SortedSet<Juggler>> assignments =
                helper.createEmptyCircuitToAssignmentsMap(circuits);
        Set<Juggler> jugglers = juggleFest.getJugglers();
        int circuitCapacity = getCircuitCapacity(juggleFest);
        Set<Juggler> residualJugglers = enlistAllJugglers(jugglers, assignments, circuitCapacity);
        assignResidualJugglers(residualJugglers, assignments, circuitCapacity);
        // use LinkedHashMap to preserve iteration order
        Map<Circuit, Set<Juggler>> typedAssignments = new LinkedHashMap<>(assignments);
        Roster roster = new Roster(typedAssignments);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Generated " + roster + " from " + juggleFest);
        }
        return roster;
    }

    private int getCircuitCapacity(JuggleFest juggleFest) {
        Set<Juggler> jugglers = juggleFest.getJugglers();
        int jugglersSize = jugglers.size();
        Set<Circuit> circuits = juggleFest.getCircuits();
        int circuitsSize = circuits.size();
        int circuitCapacity = jugglersSize / circuitsSize;
        return circuitCapacity;
    }

    private Set<Juggler> enlistAllJugglers(Set<Juggler> jugglers,
            Map<Circuit, SortedSet<Juggler>> assignments, int circuitCapacity) {
        Set<Juggler> residualJugglers = new HashSet<>();
        Map<Juggler, Integer> jugglerToNextPreferredCircuitIndexMap = new HashMap<>();
        Deque<Juggler> jugglerQueue = new LinkedList<Juggler>(jugglers);
        while (!jugglerQueue.isEmpty()) {
            Juggler juggler = jugglerQueue.removeFirst();
            int startingPreferredCircuitIndex = get(jugglerToNextPreferredCircuitIndexMap, juggler);
            int preferredCircuitIndex =
                    helper.getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(juggler,
                            startingPreferredCircuitIndex, assignments, circuitCapacity);
            List<Circuit> preferredCircuits = juggler.getPreferredCircuits();
            if (preferredCircuitIndex >= preferredCircuits.size()) {
                residualJugglers.add(juggler);
                jugglerToNextPreferredCircuitIndexMap.remove(juggler);
            } else {
                Circuit circuit = preferredCircuits.get(preferredCircuitIndex);
                SortedSet<Juggler> circuitAssignments = assignments.get(circuit);
                circuitAssignments.add(juggler);
                if (circuitAssignments.size() > circuitCapacity) {
                    Juggler jugglerWithLowestMatch = circuitAssignments.first();
                    circuitAssignments.remove(jugglerWithLowestMatch);
                    jugglerQueue.addFirst(jugglerWithLowestMatch);
                }
                jugglerToNextPreferredCircuitIndexMap.put(juggler, preferredCircuitIndex + 1);
            }
        }
        return residualJugglers;
    }

    private Integer get(Map<Juggler, Integer> map, Juggler key) {
        Integer value = map.get(key);
        if (value == null) {
            value = 0;
        }
        return value;
    }

    private void assignResidualJugglers(Set<Juggler> residualJugglers,
            Map<Circuit, SortedSet<Juggler>> assignments, int circuitCapacity) {
        if (!residualJugglers.isEmpty()) {
            helper.assignResidualJugglers(residualJugglers, assignments, circuitCapacity);
        }
    }
}
