package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl;

import java.util.Comparator;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;

public class MatchIndexJugglerComparator implements Comparator<Juggler> {
    private final MatchIndexCalculator matchIndexCalculator;
    private final Circuit circuit;

    public MatchIndexJugglerComparator(MatchIndexCalculator matchIndexCalculator, Circuit circuit) {
        this.matchIndexCalculator = matchIndexCalculator;
        this.circuit = circuit;
    }

    @Override
    public int compare(Juggler left, Juggler right) {
        int leftMatchIndex = matchIndexCalculator.calculate(circuit, left);
        int rightMatchIndex = matchIndexCalculator.calculate(circuit, right);
        int comparison = leftMatchIndex - rightMatchIndex;
        if (comparison == 0) {
            // Use the name as a fallback; only the match index matters in the actual ordering, so
            // if two jugglers have the same match index there needs to be a secondary way of
            // comparing them, otherwise they will be view as "the same" when used in a TreeSet and
            // the two will not be allowed to be remain in the set despite being different objects.
            String leftName = left.getName();
            String rightName = right.getName();
            comparison = leftName.compareTo(rightName);
        }
        return comparison;
    }
}
