package com.tylerlogic.interviews.practice.yodle.jugglefest.service.application;

import com.tylerlogic.interviews.practice.yodle.jugglefest.service.ServicePackage;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = ServicePackage.class)
public class ServiceApplicationContext {
}
