package com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;

public interface RosterService {
    Roster assignJugglersToCircuits(JuggleFest juggleFest);
}
