package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing;

import java.util.regex.Pattern;

public class ParsingConstants {
    public static final Pattern SKILLSET_DESCRIPTION_PATTERN =
            Pattern.compile("H:(\\d+) E:(\\d+) P:(\\d+)");
    public static final Pattern JUGGLER_DESCRIPTION_PATTERN =
            Pattern.compile("J (\\S+) (" + SKILLSET_DESCRIPTION_PATTERN + ") (\\S+)");

    private ParsingConstants() { }
}
