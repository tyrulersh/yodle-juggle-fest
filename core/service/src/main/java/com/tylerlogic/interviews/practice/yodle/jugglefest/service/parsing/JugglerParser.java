package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;

public interface JugglerParser {
    Set<Juggler> parse(Scanner descriptionScanner, Map<String, Circuit> circuitNameToCircuitMap);
}
