package com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.impl;

import java.util.Comparator;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexJugglerComparatorFactory;

@Named
public class MatchIndexJugglerComparatorFactoryImpl implements MatchIndexJugglerComparatorFactory {
    private final MatchIndexCalculator matchIndexCalculator;

    @Inject
    public MatchIndexJugglerComparatorFactoryImpl(MatchIndexCalculator matchIndexCalculator) {
        this.matchIndexCalculator = matchIndexCalculator;
    }

    public Comparator<Juggler> create(Circuit circuit) {
        MatchIndexJugglerComparator comparator =
                new MatchIndexJugglerComparator(matchIndexCalculator, circuit);
        return comparator;
    }
}
