package com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.impl;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexCalculator;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.JugglerPrinter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Named
public class JugglerPrinterImpl implements JugglerPrinter {
    private static final Log LOG = LogFactory.getLog(JugglerPrinterImpl.class);

    private static final char COMPONENT_SEPARATOR = ' ';
    private static final char CIRCUIT_AND_MATCH_INDEX_SEPARATOR = ':';

    private final MatchIndexCalculator matchIndexCalculator;

    @Inject
    public JugglerPrinterImpl(MatchIndexCalculator matchIndexCalculator) {
        this.matchIndexCalculator = matchIndexCalculator;
    }

    public String print(Juggler juggler) {
        String value = "";
        if (juggler != null) {
            String name = juggler.getName();
            StringBuilder builder = new StringBuilder(name);
            appendCircuitNamesWithMatchIndex(builder, juggler);
            value = builder.toString();
        }
        if (LOG.isTraceEnabled()) {
            LOG.trace("Printed " + juggler + " as " + value);
        }
        return value;
    }

    private void appendCircuitNamesWithMatchIndex(StringBuilder builder, Juggler juggler) {
        for (Circuit circuit : juggler.getPreferredCircuits()) {
            builder.append(COMPONENT_SEPARATOR);
            String name = circuit.getName();
            builder.append(name);
            builder.append(CIRCUIT_AND_MATCH_INDEX_SEPARATOR);
            int matchIndex = matchIndexCalculator.calculate(circuit, juggler);
            builder.append(matchIndex);
        }
    }
}
