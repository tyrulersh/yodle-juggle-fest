package com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;

public interface RosterServiceHelper {
    Map<Circuit, SortedSet<Juggler>> createEmptyCircuitToAssignmentsMap(Set<Circuit> circuits);

    int getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(Juggler juggler,
            int startingPreferredCircuitIndex, Map<Circuit, SortedSet<Juggler>> assignments,
            int circuitCapacity);

    void assignResidualJugglers(Set<Juggler> residualJugglers,
            Map<Circuit, SortedSet<Juggler>> assignmentsMap, int circuitCapacity);

}
