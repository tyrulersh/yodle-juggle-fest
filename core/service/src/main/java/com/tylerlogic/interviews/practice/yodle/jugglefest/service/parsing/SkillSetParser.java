package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.SkillSet;

public interface SkillSetParser {
    SkillSet parse(String description);
}
