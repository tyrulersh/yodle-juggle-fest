package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing;

import java.util.Scanner;
import java.util.Set;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;

public interface CircuitParser {
    Set<Circuit> parse(Scanner descriptionScanner);
}
