package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.CircuitParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JuggleFestParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JugglerParser;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.ParsingConstants.JUGGLER_DESCRIPTION_PATTERN;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;

@Named
public class JuggleFestParserImpl implements JuggleFestParser {
    private static final Log LOG = LogFactory.getLog(JuggleFestParserImpl.class);

    private final CircuitParser circuitParser;
    private final JugglerParser jugglerParser;

    @Inject
    public JuggleFestParserImpl(CircuitParser circuitParser, JugglerParser jugglerParser) {
        this.circuitParser = circuitParser;
        this.jugglerParser = jugglerParser;
    }

    public JuggleFest parse(Scanner descriptionScanner) {
        Set<Circuit> circuits = circuitParser.parse(descriptionScanner);
        consumeLinesUntilJugglerDescription(descriptionScanner);
        Map<String, Circuit> circuitNameToCircuitMap = createCircuitNameToCircuitMap(circuits);
        Set<Juggler> jugglers = jugglerParser.parse(descriptionScanner, circuitNameToCircuitMap);
        JuggleFest juggleFest = new JuggleFest(circuits, jugglers);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Parsed Juggle Fest: " + juggleFest);
        }
        return juggleFest;
    }

    private void consumeLinesUntilJugglerDescription(Scanner descriptionScanner) {
        descriptionScanner.useDelimiter(LINE_SEPARATOR);
        while (descriptionScanner.hasNext() &&
                !descriptionScanner.hasNext(JUGGLER_DESCRIPTION_PATTERN)) {
            descriptionScanner.next();
        }
    }

    private Map<String, Circuit> createCircuitNameToCircuitMap(Set<Circuit> circuits) {
        Map<String, Circuit> map = new HashMap<String, Circuit>();
        for (Circuit circuit : circuits) {
            String name = circuit.getName();
            map.put(name, circuit);
        }
        return map;
    }
}
