package com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing;

import java.util.Scanner;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;

public interface JuggleFestParser {
    JuggleFest parse(Scanner descriptionScanner);
}
