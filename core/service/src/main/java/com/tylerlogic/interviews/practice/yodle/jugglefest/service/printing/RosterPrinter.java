package com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;

public interface RosterPrinter {
    String print(Roster roster);
}
