package com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.impl;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Circuit;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Juggler;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.matching.MatchIndexJugglerComparatorFactory;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterServiceHelper;

@Named
public class RosterServiceHelperImpl implements RosterServiceHelper {
    private final MatchIndexJugglerComparatorFactory matchIndexJugglerComparatorFactory;

    @Inject
    public RosterServiceHelperImpl(
            MatchIndexJugglerComparatorFactory matchIndexJugglerComparatorFactory) {
        this.matchIndexJugglerComparatorFactory = matchIndexJugglerComparatorFactory;
    }

    public Map<Circuit, SortedSet<Juggler>> createEmptyCircuitToAssignmentsMap(
            Set<Circuit> circuits) {
        // use LinkedHashMap to preserve circuit iteration order
        Map<Circuit, SortedSet<Juggler>> map = new LinkedHashMap<>();
        for (Circuit circuit : circuits) {
            Comparator<Juggler> comparator = matchIndexJugglerComparatorFactory.create(circuit);
            TreeSet<Juggler> assignments = new TreeSet<>(comparator);
            map.put(circuit, assignments);
        }
        return map;
    }

    public int getFirstPreferredCircuitIndexWithCapacityOrWorseMatchedJugglers(Juggler juggler,
            int startingIndex, Map<Circuit, SortedSet<Juggler>> assignments, int capacity) {
        List<Circuit> preferredCircuits = juggler.getPreferredCircuits();
        int numberOfPreferredCircuits = preferredCircuits.size();
        int firstPreferredCircuitIndex = numberOfPreferredCircuits;
        for (int i = startingIndex; i < numberOfPreferredCircuits; i++) {
            Circuit preferredCircuit = preferredCircuits.get(i);
            SortedSet<Juggler> circuitAssignments = assignments.get(preferredCircuit);
            int currentSize = circuitAssignments.size();
            if (currentSize < capacity ||
                    isJugglerBetterThanWorstMatchedJuggler(juggler, circuitAssignments)) {
                firstPreferredCircuitIndex = i;
                break;
            }
        }
        return firstPreferredCircuitIndex;
    }

    private boolean isJugglerBetterThanWorstMatchedJuggler(Juggler juggler,
            SortedSet<Juggler> circuitAssignments) {
        Comparator<? super Juggler> comparator = circuitAssignments.comparator();
        Juggler worstMatchedJugglerAssignedToCircuit = circuitAssignments.first();
        int comparison = comparator.compare(juggler, worstMatchedJugglerAssignedToCircuit);
        boolean jugglerIsBetter = comparison > 0;
        return jugglerIsBetter;
    }

    public void assignResidualJugglers(Set<Juggler> residualJugglers,
            Map<Circuit, SortedSet<Juggler>> assignmentsMap, int circuitCapacity) {
        Queue<Juggler> jugglersToAssign = new LinkedList<>(residualJugglers);
        for (SortedSet<Juggler> assignment : assignmentsMap.values()) {
            fillCircuit(assignment, jugglersToAssign, circuitCapacity);
            if (jugglersToAssign.isEmpty()) {
                break;
            }
        }
    }

    private void fillCircuit(SortedSet<Juggler> assignment, Queue<Juggler> jugglersToAssign,
            int capacity) {
        while (assignment.size() < capacity && !jugglersToAssign.isEmpty()) {
            Juggler juggler = jugglersToAssign.remove();
            assignment.add(juggler);
        }
    }
}
