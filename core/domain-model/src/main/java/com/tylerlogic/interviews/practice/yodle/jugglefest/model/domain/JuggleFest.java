package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class JuggleFest {
    private final Set<Circuit> circuits;
    private final Set<Juggler> jugglers;

    public JuggleFest(Set<Circuit> circuits, Set<Juggler> jugglers) {
        this.circuits = circuits;
        this.jugglers = jugglers;
    }

    public Set<Circuit> getCircuits() {
        return circuits;
    }

    public Set<Juggler> getJugglers() {
        return jugglers;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(circuits);
        builder.append(jugglers);
        int hashCode = builder.toHashCode();
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if (this == obj) {
            equal = true;
        } else if (obj instanceof JuggleFest) {
            JuggleFest other = (JuggleFest) obj;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(circuits, other.circuits);
            builder.append(jugglers, other.jugglers);
            equal = builder.isEquals();
        }
        return equal;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        builder.append("circuits", circuits);
        builder.append("jugglers", jugglers);
        String stringValue = builder.toString();
        return stringValue;
    }
}
