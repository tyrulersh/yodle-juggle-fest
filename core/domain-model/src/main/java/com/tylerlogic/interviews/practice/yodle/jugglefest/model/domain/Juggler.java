package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Juggler {
    private final String name;
    private final SkillSet skillSet;
    private final List<Circuit> preferredCircuits;

    public Juggler(String name, SkillSet skillSet, List<Circuit> preferredCircuits) {
        this.name = name;
        this.skillSet = skillSet;
        this.preferredCircuits = preferredCircuits;
    }

    public String getName() {
        return name;
    }

    public SkillSet getSkillSet() {
        return skillSet;
    }

    public List<Circuit> getPreferredCircuits() {
        return preferredCircuits;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(name);
        builder.append(skillSet);
        builder.append(preferredCircuits);
        int hashCode = builder.toHashCode();
        return hashCode;
    }

    @Override
    public boolean equals(Object object) {
        boolean equal = false;
        if (this == object) {
            equal = true;
        } else if (object instanceof Juggler) {
            Juggler other = (Juggler) object;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(name, other.name);
            builder.append(skillSet, other.skillSet);
            builder.append(preferredCircuits, other.preferredCircuits);
            equal = builder.isEquals();
        }
        return equal;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        builder.append("name", name);
        builder.append("skillSet", skillSet);
        builder.append("preferredCircuits", preferredCircuits);
        String stringValue = builder.toString();
        return stringValue;
    }
}
