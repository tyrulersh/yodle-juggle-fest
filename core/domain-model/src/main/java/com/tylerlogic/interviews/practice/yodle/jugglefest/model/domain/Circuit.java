package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Circuit {
    private final String name;
    private final SkillSet skillSet;

    public Circuit(String name, SkillSet skillSet) {
        this.name = name;
        this.skillSet = skillSet;
    }

    public String getName() {
        return name;
    }

    public SkillSet getSkillSet() {
        return skillSet;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(name);
        builder.append(skillSet);
        int hashCode = builder.toHashCode();
        return hashCode;
    }

    @Override
    public boolean equals(Object object) {
        boolean equal = false;
        if (this == object) {
            equal = true;
        } else if (object instanceof Circuit) {
            Circuit other = (Circuit) object;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(name, other.name);
            builder.append(skillSet, other.skillSet);
            equal = builder.isEquals();
        }
        return equal;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        builder.append("name", name);
        builder.append("skillSet", skillSet);
        String stringValue = builder.toString();
        return stringValue;
    }
}
