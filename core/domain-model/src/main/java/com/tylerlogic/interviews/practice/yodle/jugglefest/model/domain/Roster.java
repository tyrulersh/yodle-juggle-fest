package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Roster {
    private final Map<Circuit, Set<Juggler>> assignments;

    public Roster(Map<Circuit, Set<Juggler>> assignments) {
        this.assignments = assignments;
    }

    public Map<Circuit, Set<Juggler>> getAssignments() {
        return assignments;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(assignments);
        int hashCode = builder.toHashCode();
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if (this == obj) {
            equal = true;
        } else if (obj instanceof Roster) {
            Roster other = (Roster) obj;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(assignments, other.assignments);
            equal = builder.isEquals();
        }
        return equal;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        builder.append("assignments", assignments);
        String stringValue = builder.toString();
        return stringValue;
    }
}
