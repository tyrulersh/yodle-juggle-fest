package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class SkillSet {
    private final int handEyeCoordination;
    private final int endurance;
    private final int pizazz;

    public SkillSet(int handEyeCoordination, int endurance, int pizazz) {
        this.handEyeCoordination = handEyeCoordination;
        this.endurance = endurance;
        this.pizazz = pizazz;
    }

    public int getHandEyeCoordination() {
        return handEyeCoordination;
    }

    public int getEndurance() {
        return endurance;
    }

    public int getPizazz() {
        return pizazz;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(handEyeCoordination);
        builder.append(endurance);
        builder.append(pizazz);
        int hashCode = builder.toHashCode();
        return hashCode;
    }

    @Override
    public boolean equals(Object object) {
        boolean equal = false;
        if (this == object) {
            equal = true;
        } else if (object instanceof SkillSet) {
            SkillSet other = (SkillSet) object;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(handEyeCoordination, other.handEyeCoordination);
            builder.append(endurance, other.endurance);
            builder.append(pizazz, other.pizazz);
            equal = builder.isEquals();
        }
        return equal;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        builder.append("handEyeCoordination", handEyeCoordination);
        builder.append("endurance", endurance);
        builder.append("pizazz", pizazz);
        String stringValue = builder.toString();
        return stringValue;
    }
}
