package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class SkillSetTest {
    private static final int HAND_EYE_COORDINATION = 123;
    private static final int ENDURANCE = 456;
    private static final int PIZAZZ = 789;

    private SkillSet skillSet = new SkillSet(HAND_EYE_COORDINATION, ENDURANCE, PIZAZZ);

    @Test
    public void getters() {
        assertThat(skillSet.getHandEyeCoordination(), is(HAND_EYE_COORDINATION));
        assertThat(skillSet.getEndurance(), is(ENDURANCE));
        assertThat(skillSet.getPizazz(), is(PIZAZZ));
    }

    @Test
    public void notEqualToObjectOfDifferentType() {
        assertThat(skillSet.equals("object of different type"), is(false));
    }

    @Test
    public void equalToSameInstance() {
        assertThat(skillSet.equals(skillSet), is(true));
    }

    @Test
    public void equalToEquivalentObject() {
        SkillSet equivalentSkillSet = new SkillSet(HAND_EYE_COORDINATION, ENDURANCE, PIZAZZ);
        assertThat(skillSet.equals(equivalentSkillSet), is(true));
        assertThat(equivalentSkillSet.equals(skillSet), is(true));
    }

    @Test
    public void notEqualWhenHandEyeCoordinationAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(HAND_EYE_COORDINATION + 1, ENDURANCE, PIZAZZ);
        assertThat(skillSet.equals(differentSkillSet), is(false));
        assertThat(differentSkillSet.equals(skillSet), is(false));
    }

    @Test
    public void notEqualWhenEnduranceAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(HAND_EYE_COORDINATION, ENDURANCE + 1, PIZAZZ);
        assertThat(skillSet.equals(differentSkillSet), is(false));
        assertThat(differentSkillSet.equals(skillSet), is(false));
    }

    @Test
    public void notEqualWhenPizazzAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(HAND_EYE_COORDINATION, ENDURANCE, PIZAZZ + 1);
        assertThat(skillSet.equals(differentSkillSet), is(false));
        assertThat(differentSkillSet.equals(skillSet), is(false));
    }

    @Test
    public void hashCodeIsTheSameForTheSameInstance() {
        assertThat(skillSet.hashCode(), is(skillSet.hashCode()));
    }

    @Test
    public void hashCodeForEquivalentObjectsAreEqual() {
        SkillSet equivalentSkillSet = new SkillSet(HAND_EYE_COORDINATION, ENDURANCE, PIZAZZ);
        assertThat(skillSet.hashCode(), is(equivalentSkillSet.hashCode()));
    }

    @Test
    public void hashCodeIsDifferentWhenHandEyeCoordinationAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(HAND_EYE_COORDINATION + 1, ENDURANCE, PIZAZZ);
        assertThat(skillSet.hashCode(), is(not(differentSkillSet.hashCode())));
    }

    @Test
    public void hashCodeIsDifferentWhenEnduranceAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(HAND_EYE_COORDINATION, ENDURANCE + 1, PIZAZZ);
        assertThat(skillSet.hashCode(), is(not(differentSkillSet.hashCode())));
    }

    @Test
    public void hashCodeIsDifferentWhenPizazzAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(HAND_EYE_COORDINATION, ENDURANCE, PIZAZZ + 1);
        assertThat(skillSet.hashCode(), is(not(differentSkillSet.hashCode())));
    }

    @Test
    public void toStringCorrectness() {
        String expectedStringValue = SkillSet.class.getSimpleName() + "[" +
                "handEyeCoordination=" + HAND_EYE_COORDINATION + "," +
                "endurance=" + ENDURANCE + "," +
                "pizazz=" + PIZAZZ + "]";
        assertThat(skillSet.toString(), is(equalTo(expectedStringValue)));
    }
}
