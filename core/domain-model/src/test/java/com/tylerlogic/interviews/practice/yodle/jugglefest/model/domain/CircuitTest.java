package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class CircuitTest {
    private static final String NAME = "name";
    private static final SkillSet SKILL_SET = new SkillSet(123, 456, 789);

    private final Circuit circuit = new Circuit(NAME, SKILL_SET);

    @Test
    public void getters() {
        assertThat(circuit.getName(), is(sameInstance(NAME)));
        assertThat(circuit.getSkillSet(), is(sameInstance(SKILL_SET)));
    }

    @Test
    public void notEqualToObjectOfDifferentType() {
        assertThat(circuit.equals("object of different type"), is(false));
    }

    @Test
    public void equalToSameInstance() {
        assertThat(circuit.equals(circuit), is(true));
    }

    @Test
    public void equalToEquivalentObject() {
        Circuit equivalentCircuit = new Circuit(NAME, SKILL_SET);
        assertThat(circuit.equals(equivalentCircuit), is(true));
        assertThat(equivalentCircuit.equals(circuit), is(true));
    }

    @Test
    public void notEqualWhenNamesAreDifferent() {
        Circuit differentCircuit = new Circuit(NAME + "extra", SKILL_SET);
        assertThat(circuit.equals(differentCircuit), is(false));
        assertThat(differentCircuit.equals(circuit), is(false));
    }

    @Test
    public void notEqualWhenSkillSetsAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(SKILL_SET.getHandEyeCoordination() + 1, 123, 456);
        Circuit differentCircuit = new Circuit(NAME, differentSkillSet);
        assertThat(circuit.equals(differentCircuit), is(false));
        assertThat(differentCircuit.equals(circuit), is(false));
    }

    @Test
    public void hashCodeIsTheSameForTheSameInstance() {
        assertThat(circuit.hashCode(), is(circuit.hashCode()));
    }

    @Test
    public void hashCodeForEquivalentObjectsAreEqual() {
        Circuit equivalentCircuit = new Circuit(NAME, SKILL_SET);
        assertThat(circuit.hashCode(), is(equivalentCircuit.hashCode()));
    }

    @Test
    public void hashCodeIsDifferentWhenNamesAreDifferent() {
        Circuit differentCircuit = new Circuit(NAME + "extra", SKILL_SET);
        assertThat(circuit.hashCode(), is(not(differentCircuit.hashCode())));
    }

    @Test
    public void hashCodeIsDifferentWhenSkillSetsAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(SKILL_SET.getHandEyeCoordination() + 1, 123, 456);
        Circuit differentCircuit = new Circuit(NAME, differentSkillSet);
        assertThat(circuit.hashCode(), is(not(differentCircuit.hashCode())));
    }

    @Test
    public void toStringCorrectness() {
        String expectedStringValue = Circuit.class.getSimpleName() + "[" +
                "name=" + NAME + "," +
                "skillSet=" + SKILL_SET + "]";
        assertThat(circuit.toString(), is(equalTo(expectedStringValue)));
    }
}
