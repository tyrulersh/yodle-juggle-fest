package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import java.util.Set;

import static java.util.Arrays.asList;

import static com.google.common.collect.Sets.newHashSet;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class JuggleFestTest {
    private static final Circuit CIRCUIT1 = new Circuit("C1", new SkillSet(123, 456, 789));
    private static final Circuit CIRCUIT2 = new Circuit("C2", new SkillSet(456, 789, 123));
    private static final Circuit CIRCUIT3 = new Circuit("C3", new SkillSet(789, 123, 456));

    private static final Juggler JUGGLER1 =
            new Juggler("J1", new SkillSet(123, 456, 789), asList(CIRCUIT1));
    private static final Juggler JUGGLER2 =
            new Juggler("J2", new SkillSet(456, 789, 123), asList(CIRCUIT2, CIRCUIT3));
    private static final Juggler JUGGLER3 =
            new Juggler("J3", new SkillSet(789, 123, 456), asList(CIRCUIT1, CIRCUIT2, CIRCUIT3));

    private static final Set<Circuit> CIRCUITS = newHashSet(CIRCUIT1, CIRCUIT2, CIRCUIT3);
    private static final Set<Juggler> JUGGLERS = newHashSet(JUGGLER1, JUGGLER2, JUGGLER3);
    private final JuggleFest juggleFest = new JuggleFest(CIRCUITS, JUGGLERS);

    @Test
    public void getters() {
        assertThat(juggleFest.getCircuits(), is(sameInstance(CIRCUITS)));
        assertThat(juggleFest.getJugglers(), is(sameInstance(JUGGLERS)));
    }

    @Test
    public void notEqualToObjectOfDifferentType() {
        assertThat(juggleFest.equals("object of different type"), is(false));
    }

    @Test
    public void equalToSameInstance() {
        assertThat(juggleFest.equals(juggleFest), is(true));
    }

    @Test
    public void equalToEquivalentObject() {
        JuggleFest equivalentJuggleFest = new JuggleFest(CIRCUITS, JUGGLERS);
        assertThat(juggleFest.equals(equivalentJuggleFest), is(true));
        assertThat(equivalentJuggleFest.equals(juggleFest), is(true));
    }

    @Test
    public void notEqualWhenCircuitsAreDifferent() {
        JuggleFest differentJuggleFest = new JuggleFest(newHashSet(CIRCUIT1), JUGGLERS);
        assertThat(juggleFest.equals(differentJuggleFest), is(false));
        assertThat(differentJuggleFest.equals(juggleFest), is(false));
    }

    @Test
    public void notEqualWhenJugglersAreDifferent() {
        JuggleFest differentJuggleFest = new JuggleFest(CIRCUITS, newHashSet(JUGGLER1));
        assertThat(juggleFest.equals(differentJuggleFest), is(false));
        assertThat(differentJuggleFest.equals(juggleFest), is(false));
    }

    @Test
    public void hashCodeIsTheSameForTheSameInstance() {
        assertThat(juggleFest.hashCode(), is(juggleFest.hashCode()));
    }

    @Test
    public void hashCodeForEquivalentObjectsAreEqual() {
        JuggleFest equivalentJuggleFest = new JuggleFest(CIRCUITS, JUGGLERS);
        assertThat(juggleFest.hashCode(), is(equivalentJuggleFest.hashCode()));
    }

    @Test
    public void hashCodeIsDifferentWhenCircuitsAreDifferent() {
        JuggleFest differentJuggleFest = new JuggleFest(newHashSet(CIRCUIT1), JUGGLERS);
        assertThat(juggleFest.hashCode(), is(not(differentJuggleFest.hashCode())));
    }

    @Test
    public void hashCodeIsDifferentWhenJugglersAreDifferent() {
        JuggleFest differentJuggleFest = new JuggleFest(CIRCUITS, newHashSet(JUGGLER1));
        assertThat(juggleFest.hashCode(), is(not(differentJuggleFest.hashCode())));
    }

    @Test
    public void toStringCorrectness() {
        String expectedStringValue = JuggleFest.class.getSimpleName() + "[" +
                "circuits=" + CIRCUITS + "," +
                "jugglers=" + JUGGLERS + "]";
        assertThat(juggleFest.toString(), is(equalTo(expectedStringValue)));
    }
}
