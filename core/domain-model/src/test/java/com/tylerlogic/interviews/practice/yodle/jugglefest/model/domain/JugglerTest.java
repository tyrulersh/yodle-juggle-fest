package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class JugglerTest {
    private static final String NAME = "name";
    private static final SkillSet SKILL_SET = new SkillSet(123, 456, 789);
    private static final List<Circuit> PREFERRED_CIRCUITS
            = Arrays.asList(new Circuit("C1", null), new Circuit("C2", null));

    private Juggler juggler = new Juggler(NAME, SKILL_SET, PREFERRED_CIRCUITS);

    @Test
    public void getters() {
        assertThat(juggler.getName(), is(sameInstance(NAME)));
        assertThat(juggler.getSkillSet(), is(sameInstance(SKILL_SET)));
        assertThat(juggler.getPreferredCircuits(), is(sameInstance(PREFERRED_CIRCUITS)));
    }

    @Test
    public void notEqualToObjectOfDifferentType() {
        assertThat(juggler.equals("object of different type"), is(false));
    }

    @Test
    public void equalToSameInstance() {
        assertThat(juggler.equals(juggler), is(true));
    }

    @Test
    public void equalToEquivalentObject() {
        Juggler equivalentJuggler = new Juggler(NAME, SKILL_SET, PREFERRED_CIRCUITS);
        assertThat(juggler.equals(equivalentJuggler), is(true));
        assertThat(equivalentJuggler.equals(juggler), is(true));
    }

    @Test
    public void notEqualWhenNameAreDifferent() {
        Juggler differentJuggler = new Juggler(NAME + "extra", SKILL_SET, PREFERRED_CIRCUITS);
        assertThat(juggler.equals(differentJuggler), is(false));
        assertThat(differentJuggler.equals(juggler), is(false));
    }

    @Test
    public void notEqualWhenSkillSetAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(SKILL_SET.getHandEyeCoordination() + 1, 123, 456);
        Juggler differentJuggler = new Juggler(NAME, differentSkillSet, PREFERRED_CIRCUITS);
        assertThat(juggler.equals(differentJuggler), is(false));
        assertThat(differentJuggler.equals(juggler), is(false));
    }

    @Test
    public void notEqualWhenPreferredCircuitsAreDifferent() {
        List<Circuit> differentPreferredCircuits = new ArrayList<Circuit>(PREFERRED_CIRCUITS);
        differentPreferredCircuits.add(new Circuit("extra", null));
        Juggler differentJuggler = new Juggler(NAME, SKILL_SET, differentPreferredCircuits);
        assertThat(juggler.equals(differentJuggler), is(false));
        assertThat(differentJuggler.equals(juggler), is(false));
    }

    @Test
    public void hashCodeIsTheSameForTheSameInstance() {
        assertThat(juggler.hashCode(), is(juggler.hashCode()));
    }

    @Test
    public void hashCodeForEquivalentObjectsAreEqual() {
        Juggler equivalentJuggler = new Juggler(NAME, SKILL_SET, PREFERRED_CIRCUITS);
        assertThat(juggler.hashCode(), is(equivalentJuggler.hashCode()));
    }

    @Test
    public void hashCodeIsDifferentWhenNameAreDifferent() {
        Juggler differentJuggler = new Juggler(NAME + "extra", SKILL_SET, PREFERRED_CIRCUITS);
        assertThat(juggler.hashCode(), is(not(differentJuggler.hashCode())));
    }

    @Test
    public void hashCodeIsDifferentWhenSkillSetAreDifferent() {
        SkillSet differentSkillSet = new SkillSet(SKILL_SET.getHandEyeCoordination() + 1, 123, 456);
        Juggler differentJuggler = new Juggler(NAME, differentSkillSet, PREFERRED_CIRCUITS);
        assertThat(juggler.hashCode(), is(not(differentJuggler.hashCode())));
    }

    @Test
    public void hashCodeIsDifferentWhenPreferredCircuitsAreDifferent() {
        List<Circuit> differentPreferredCircuits = new ArrayList<Circuit>(PREFERRED_CIRCUITS);
        differentPreferredCircuits.add(new Circuit("extra", null));
        Juggler differentJuggler = new Juggler(NAME, SKILL_SET, differentPreferredCircuits);
        assertThat(juggler.hashCode(), is(not(differentJuggler.hashCode())));
    }

    @Test
    public void toStringCorrectness() {
        String expectedStringValue = Juggler.class.getSimpleName() + "[" +
                "name=" + NAME + "," +
                "skillSet=" + SKILL_SET + "," +
                "preferredCircuits=" + PREFERRED_CIRCUITS + "]";
        assertThat(juggler.toString(), is(equalTo(expectedStringValue)));
    }
}
