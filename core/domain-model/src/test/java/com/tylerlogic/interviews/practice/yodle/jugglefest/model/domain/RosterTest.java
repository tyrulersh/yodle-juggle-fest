package com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class RosterTest {
    private static final Circuit CIRCUIT1 = new Circuit("C1", null);
    private static final Circuit CIRCUIT2 = new Circuit("C2", null);

    private static final Juggler JUGGLER1 = new Juggler("J1", null, null);
    private static final Juggler JUGGLER2 = new Juggler("J2", null, null);

    private static final Map<Circuit, Set<Juggler>> ASSIGNMENTS = new HashMap<>();

    static {
        ASSIGNMENTS.put(CIRCUIT1, newHashSet(JUGGLER1));
        ASSIGNMENTS.put(CIRCUIT2, newHashSet(JUGGLER1, JUGGLER2));
    }

    private Roster roster = new Roster(ASSIGNMENTS);

    @Test
    public void getters() {
        assertThat(roster.getAssignments(), is(sameInstance(ASSIGNMENTS)));
    }

    @Test
    public void notEqualToObjectOfDifferentType() {
        assertThat(roster.equals("object of different type"), is(false));
    }

    @Test
    public void equalToSameInstance() {
        assertThat(roster.equals(roster), is(true));
    }

    @Test
    public void equalToEquivalentObject() {
        Roster equivalentRoster = new Roster(ASSIGNMENTS);
        assertThat(roster.equals(equivalentRoster), is(true));
        assertThat(equivalentRoster.equals(roster), is(true));
    }

    @Test
    public void notEqualWhenAssignmetnsAreDifferent() {
        Roster differentRoster = new Roster(Collections.emptyMap());
        assertThat(roster.equals(differentRoster), is(false));
        assertThat(differentRoster.equals(roster), is(false));
    }

    @Test
    public void hashCodeIsTheSameForTheSameInstance() {
        assertThat(roster.hashCode(), is(roster.hashCode()));
    }

    @Test
    public void hashCodeForEquivalentObjectsAreEqual() {
        Roster equivalentRoster = new Roster(ASSIGNMENTS);
        assertThat(roster.hashCode(), is(equivalentRoster.hashCode()));
    }

    @Test
    public void hashCodeIsDifferentWhenAssignmentsAreDifferent() {
        Roster differentRoster = new Roster(Collections.emptyMap());
        assertThat(roster.hashCode(), is(not(differentRoster.hashCode())));
    }

    @Test
    public void toStringCorrectness() {
        String expectedStringValue = Roster.class.getSimpleName() + "[" +
                "assignments=" + ASSIGNMENTS + "]";
        assertThat(roster.toString(), is(equalTo(expectedStringValue)));
    }
}
