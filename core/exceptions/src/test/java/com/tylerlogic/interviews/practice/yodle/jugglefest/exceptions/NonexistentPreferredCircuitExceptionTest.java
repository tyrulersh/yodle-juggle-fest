package com.tylerlogic.interviews.practice.yodle.jugglefest.exceptions;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class NonexistentPreferredCircuitExceptionTest {
    private static final String JUGGLER_NAME = "juggler";
    private static final String NON_EXISTENT_CIRCUIT_NAME = "nonexistentCircuit";

    private NonexistentPreferredCircuitException exception =
            new NonexistentPreferredCircuitException(JUGGLER_NAME, NON_EXISTENT_CIRCUIT_NAME);

    @Test
    public void getMessage() {
        assertThat(exception.getMessage(), is(equalTo("Juggler '" + JUGGLER_NAME +
                "' prefers nonexistent circuit: '" + NON_EXISTENT_CIRCUIT_NAME + "'")));
    }
}
