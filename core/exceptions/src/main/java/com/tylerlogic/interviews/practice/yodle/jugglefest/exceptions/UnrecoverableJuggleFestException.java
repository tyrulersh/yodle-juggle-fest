package com.tylerlogic.interviews.practice.yodle.jugglefest.exceptions;

/**
 * "Marker class" for catching unrecoverable exceptions specific to any JuggleFest application.
 */
@SuppressWarnings("serial")
public class UnrecoverableJuggleFestException extends RuntimeException {
    public UnrecoverableJuggleFestException(String message) {
        super(message);
    }
}
