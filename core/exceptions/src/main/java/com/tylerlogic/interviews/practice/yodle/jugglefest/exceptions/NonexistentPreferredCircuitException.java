package com.tylerlogic.interviews.practice.yodle.jugglefest.exceptions;

@SuppressWarnings("serial")
public class NonexistentPreferredCircuitException extends UnrecoverableJuggleFestException {
    public NonexistentPreferredCircuitException(String jugglerName, String nonExistentCircuitName) {
        super("Juggler '" + jugglerName + "' prefers nonexistent circuit: '" +
                nonExistentCircuitName + "'");
    }
}
