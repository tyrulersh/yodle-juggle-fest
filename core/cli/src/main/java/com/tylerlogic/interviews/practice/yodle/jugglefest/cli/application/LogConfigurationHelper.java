package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.impl.Log4JLogger;
import org.apache.commons.logging.impl.NoOpLog;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.core.layout.PatternLayout.Builder;

import static org.apache.commons.logging.impl.LogFactoryImpl.LOG_PROPERTY;
import static org.apache.logging.log4j.LogManager.ROOT_LOGGER_NAME;
import static org.apache.logging.log4j.core.layout.PatternLayout.SIMPLE_CONVERSION_PATTERN;

public class LogConfigurationHelper {
    private static final String FILE_APPENDER_NAME = "JuggleFestFileLogger";
    private static final String STATUS_LOGGER_LEVEL_SYSTEM_PROPERTY =
            "org.apache.logging.log4j.simplelog.StatusLogger.level";

    public void turnOffLoggingSystemStatusLogging() {
        System.setProperty(STATUS_LOGGER_LEVEL_SYSTEM_PROPERTY, "OFF");
    }

    public void turnOffLogging() {
        LogFactory logFactory = LogFactory.getFactory();
        logFactory.setAttribute(LOG_PROPERTY, NoOpLog.class.getName());
    }

    public void configureLoggingToFile(String logFilePath, Level level) {
        LogFactory logFactory = LogFactory.getFactory();
        logFactory.setAttribute(LOG_PROPERTY, Log4JLogger.class.getName());
        configureRootLoggerWithFileAppender(logFilePath, level);
    }

    private void configureRootLoggerWithFileAppender(String logFilePath, Level level) {
        LoggerContext loggerContext = (LoggerContext)LogManager.getContext(false);
        Configuration configuration = loggerContext.getConfiguration();
        PatternLayout layout = createPatternLayout(configuration);
        Appender fileAppender = createFileAppender(logFilePath, configuration, layout);
        fileAppender.start();
        configuration.addAppender(fileAppender);
        AppenderRef[] refs = createAppenderRefs();
        LoggerConfig loggerConfig = LoggerConfig.createLogger("false", level, ROOT_LOGGER_NAME,
                "true", refs, null, configuration, null);
        loggerConfig.addAppender(fileAppender, null, null);
        configuration.addLogger(ROOT_LOGGER_NAME, loggerConfig);
        loggerContext.updateLoggers();
    }

    private PatternLayout createPatternLayout(Configuration configuration) {
        Builder builder = PatternLayout.newBuilder();
        builder.withConfiguration(configuration);
        builder.withPattern(SIMPLE_CONVERSION_PATTERN);
        PatternLayout layout = builder.build();
        return layout;
    }

    private FileAppender createFileAppender(String logFilePath, Configuration configuration,
            PatternLayout layout) {
        FileAppender appender = FileAppender.createAppender(logFilePath, "false", "false",
                FILE_APPENDER_NAME, "true", "false", "false", "4000", layout, null, "false", null,
                configuration);
        return appender;
    }

    private AppenderRef[] createAppenderRefs() {
        AppenderRef fileAppenderRef = AppenderRef.createAppenderRef(FILE_APPENDER_NAME, null, null);
        AppenderRef[] refs = new AppenderRef[] { fileAppenderRef };
        return refs;
    }
}
