package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;

public class CommandLineConstants {
    public static final String PROGRAM_NAME = "jf";
    public static final String CLI_USAGE = PROGRAM_NAME + " [OPTION]... [FILE]";
    public static final String CLI_DESCRIPTION =
            "Reads the description of circuits and jugglers from FILE and writes the" +
                    LINE_SEPARATOR +
            "assignments for the Yodle Juggle Fest to STDOUT. The argument FILE is optional," +
                    LINE_SEPARATOR +
            "and, if not provided, STDIN will be read instead." + LINE_SEPARATOR +
            LINE_SEPARATOR +
            "Options:";

    public static final String HELP_OPTION_NAME = "help";
    public static final String OUTPUT_OPTION_NAME = "output";
    public static final String VERBOSE_OPTION_NAME = "verbose";

    public static final Options CLI_OPTIONS = new Options();

    static {
        CLI_OPTIONS.addOption("h", HELP_OPTION_NAME, false, "Print this help message.");
        CLI_OPTIONS.addOption(
                Option.builder("o").longOpt(OUTPUT_OPTION_NAME).hasArg().argName("FILE").desc(
                        "Writes all levels of logging information to FILE.").build());
        CLI_OPTIONS.addOption("v", VERBOSE_OPTION_NAME, false,
                "Verbose output in log file. This can be specified multiple times, but anything " +
                "beyond two is neglected. This option is useless without writing to a log file.");
    }

    public static final HelpFormatter HELP_FORMATTER = new HelpFormatter();

    static {
        HELP_FORMATTER.setWidth(80);
        HELP_FORMATTER.setLeftPadding(2);
        HELP_FORMATTER.setDescPadding(4);
        HELP_FORMATTER.setSyntaxPrefix("Usage: ");
    }

    private CommandLineConstants() {
    }
}
