package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import com.tylerlogic.interviews.practice.yodle.jugglefest.cli.JuggleFestProcessor;
import com.tylerlogic.interviews.practice.yodle.jugglefest.exceptions.UnrecoverableJuggleFestException;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.CLI_DESCRIPTION;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.CLI_OPTIONS;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.CLI_USAGE;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.HELP_FORMATTER;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.HELP_OPTION_NAME;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.OUTPUT_OPTION_NAME;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.PROGRAM_NAME;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.CommandLineConstants.VERBOSE_OPTION_NAME;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;

public class Driver {
    private static final String ERROR_HELP_MESSAGE =
            "Try '" + PROGRAM_NAME + " --help' for more information.";
    /**
     * Levels are in order of increased verbosity.
     */
    private static final List<Level> LOGGING_LEVELS =
            Arrays.asList(Level.INFO, Level.DEBUG, Level.TRACE);

    private static LogConfigurationHelper logConfigurationHelper = new LogConfigurationHelper();

    public static void main(String[] arguments) {
        CommandLine commandLine = parseArguments(arguments);
        List<String> positionalArguments = commandLine.getArgList();
        if (positionalArguments.size() <= 1) {
            if (commandLine.hasOption(HELP_OPTION_NAME)) {
                HELP_FORMATTER.printHelp(CLI_USAGE, CLI_DESCRIPTION, CLI_OPTIONS, null);
            } else {
                setupLogging(commandLine);
                try {
                    processJuggleFest(positionalArguments);
                } catch (UnrecoverableJuggleFestException exception) {
                    String message = exception.getMessage();
                    logError(message);
                    System.exit(1);
                }
            }
        } else {
            String argumentsLine = StringUtils.join(positionalArguments, ' ');
            logError("Program takes one argument at most, instead got: " + argumentsLine +
                    LINE_SEPARATOR + ERROR_HELP_MESSAGE);
            int numberOfPositionalArguments = positionalArguments.size();
            System.exit(numberOfPositionalArguments);
        }
    }

    private static CommandLine parseArguments(String[] arguments) {
        CommandLine commandLine = null;
        try {
            CommandLineParser parser = new DefaultParser();
            commandLine = parser.parse(CLI_OPTIONS, arguments);
        } catch (ParseException parseException) {
            String message = parseException.getMessage();
            logError(message + LINE_SEPARATOR + ERROR_HELP_MESSAGE);
            System.exit(1);
        }
        return commandLine;
    }

    private static void logError(String message) {
        System.err.println(PROGRAM_NAME + ": " + message);
    }

    private static void setupLogging(CommandLine commandLine) {
        /*
         * When using log4j, if there is no log4j configuration XML file available, the logging
         * system's status logger logs an error stating such to STDERR. Since this program does not
         * use a log4j configuration file, there's no need for that message to be displayed, and so
         * the level of StatusLogger is set to off.
         */
        logConfigurationHelper.turnOffLoggingSystemStatusLogging();
        if (commandLine.hasOption(OUTPUT_OPTION_NAME)) {
            String logFilePath = commandLine.getOptionValue(OUTPUT_OPTION_NAME);
            Level level = resolveLoggingLevel(commandLine);
            logConfigurationHelper.configureLoggingToFile(logFilePath, level);
        } else {
            logConfigurationHelper.turnOffLogging();
        }
    }

    private static Level resolveLoggingLevel(CommandLine commandLine) {
        int numberOfVerboseOptions = getOptionMultiplicity(VERBOSE_OPTION_NAME, commandLine);
        int loggingLevelIndex = Math.min(numberOfVerboseOptions, LOGGING_LEVELS.size() - 1);
        Level level = LOGGING_LEVELS.get(loggingLevelIndex);
        return level;
    }

    private static int getOptionMultiplicity(String optionName, CommandLine commandLine) {
        int count = 0;
        for (Option option : commandLine.getOptions()) {
            if (option.getLongOpt().equals(optionName)) {
                ++count;
            }
        }
        return count;
    }

    private static void processJuggleFest(List<String> arguments) {
        InputStream inputStream = resolveInputStream(arguments);
        ConfigurableApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(CliApplicationContext.class);
        JuggleFestProcessor juggleFestProcessor =
                applicationContext.getBean(JuggleFestProcessor.class);
        juggleFestProcessor.solve(inputStream, System.out);
        System.out.println();
        applicationContext.close();
    }

    private static InputStream resolveInputStream(List<String> arguments) {
        InputStream inputStream = System.in;
        if (!arguments.isEmpty()) {
            String filename = arguments.get(0);
            File inputFile = new File(filename);
            try {
                inputStream = new FileInputStream(inputFile);
            } catch (FileNotFoundException exception) {
                logError("No such file: " + inputFile);
                System.exit(1);
            }
        }
        return inputStream;
    }
}
