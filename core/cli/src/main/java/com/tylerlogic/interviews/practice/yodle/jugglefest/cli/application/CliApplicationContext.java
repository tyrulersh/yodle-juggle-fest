package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application;

import com.tylerlogic.interviews.practice.yodle.jugglefest.cli.CliPackage;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.application.ServiceApplicationContext;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ServiceApplicationContext.class)
@ComponentScan(basePackageClasses = CliPackage.class)
public class CliApplicationContext {
}
