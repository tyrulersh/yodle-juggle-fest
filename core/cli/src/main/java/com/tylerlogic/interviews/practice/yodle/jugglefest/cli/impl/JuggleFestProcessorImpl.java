package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.impl;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import javax.inject.Inject;
import javax.inject.Named;

import com.tylerlogic.interviews.practice.yodle.jugglefest.cli.JuggleFestProcessor;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JuggleFestParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.RosterPrinter;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterService;

@Named
public class JuggleFestProcessorImpl implements JuggleFestProcessor {
    private final JuggleFestParser juggleFestParser;
    private final RosterService rosterService;
    private final RosterPrinter rosterPrinter;

    @Inject
    public JuggleFestProcessorImpl(JuggleFestParser juggleFestParser, RosterService rosterService,
            RosterPrinter rosterPrinter) {
        this.juggleFestParser = juggleFestParser;
        this.rosterService = rosterService;
        this.rosterPrinter = rosterPrinter;
    }

    public void solve(InputStream input, PrintStream output) {
        Scanner descriptionScanner = new Scanner(input);
        JuggleFest juggleFest = juggleFestParser.parse(descriptionScanner);
        Roster roster = rosterService.assignJugglersToCircuits(juggleFest);
        String rosterString = rosterPrinter.print(roster);
        output.print(rosterString);
    }
}
