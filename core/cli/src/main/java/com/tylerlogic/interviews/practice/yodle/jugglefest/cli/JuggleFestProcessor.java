package com.tylerlogic.interviews.practice.yodle.jugglefest.cli;

import java.io.InputStream;
import java.io.PrintStream;

public interface JuggleFestProcessor {
    /**
     * Reads the problem description from input and writes the solution to the output.
     */
    void solve(InputStream input, PrintStream output);
}
