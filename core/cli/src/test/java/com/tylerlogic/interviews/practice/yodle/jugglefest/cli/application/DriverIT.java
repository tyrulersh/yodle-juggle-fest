package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.EXPECTED_HELP_MESSAGE;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.NL;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DriverIT {
    private ByteArrayOutputStream mockOut = new ByteArrayOutputStream();
    private ByteArrayOutputStream mockErr = new ByteArrayOutputStream();

    private InputStream preservedSystemIn;
    private PrintStream preservedSystemOut;
    private PrintStream preservedSystemErr;

    @Before
    public void recordSystemInsAndOuts() {
        preservedSystemIn = System.in;
        preservedSystemOut = System.out;
        System.setOut(new PrintStream(mockOut));
        preservedSystemErr = System.err;
        System.setErr(new PrintStream(mockErr));
    }

    @After
    public void resetSystemInsAndOuts() {
        System.setIn(preservedSystemIn);
        System.setOut(preservedSystemOut);
        System.setErr(preservedSystemErr);
    }

    @Test
    public void printHelpUsingShortOption() {
        Driver.main(new String[] { "-h" });
        assertThat(mockOut.toString(), is(equalTo(EXPECTED_HELP_MESSAGE + NL)));
        assertThat("Nothing is expected on STDERR", mockErr.toString(), is(equalTo("")));
    }

    @Test
    public void printHelpUsingLongOption() {
        Driver.main(new String[] { "--help" });
        assertThat(mockOut.toString(), is(equalTo(EXPECTED_HELP_MESSAGE + NL)));
        assertThat("Nothing is expected on STDERR", mockErr.toString(), is(equalTo("")));
    }

    @Test
    public void jugglersAssignedToTheirPreferredCircuits() {
        String[] inputLines = new String[] {
                "C C1 H:1 E:1 P:1",
                "C C2 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C2",
                "J J2 H:1 E:1 P:1 C1"
        };
        String input = StringUtils.join(inputLines, NL);
        System.setIn(new ByteArrayInputStream(input.getBytes()));
        Driver.main(null);
        assertThat(mockOut.toString(), is(equalTo(
                "C1 J2 C1:3" + NL +
                "C2 J1 C2:3" + NL)));
        assertThat("Nothing is expected on STDERR", mockErr.toString(), is(equalTo("")));
    }

    @Test
    public void jugglersAssignedToThePreferredCircuitsTheyMatchBest() {
        String[] inputLines = new String[] {
                "C C1 H:1 E:1 P:1",
                "C C2 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C1",
                "J J2 H:2 E:2 P:2 C1",
                "J J3 H:3 E:3 P:3 C1",
                "J J4 H:4 E:4 P:4 C1"
        };
        String input = StringUtils.join(inputLines, NL);
        System.setIn(new ByteArrayInputStream(input.getBytes()));
        Driver.main(null);
        assertThat(mockOut.toString(), is(equalTo(
                "C1 J3 C1:9, J4 C1:12" + NL +
                "C2 J1 C1:3, J2 C1:6" + NL)));
        assertThat("Nothing is expected on STDERR", mockErr.toString(), is(equalTo("")));
    }
}
