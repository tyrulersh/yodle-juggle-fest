package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application;

import static org.apache.commons.lang3.SystemUtils.LINE_SEPARATOR;

public class IntegrationTestConstants {
    // abbreviated for improved readability of test code
    public static final String NL = LINE_SEPARATOR;

    public static final String EXPECTED_HELP_MESSAGE =
            "Usage: jf [OPTION]... [FILE]" + NL +
            "Reads the description of circuits and jugglers from FILE and writes the" + NL +
            "assignments for the Yodle Juggle Fest to STDOUT. The argument FILE is optional," + NL +
            "and, if not provided, STDIN will be read instead." + NL +
            NL +
            "Options:" + NL +
            "  -h,--help             Print this help message." + NL +
            "  -o,--output <FILE>    Writes all levels of logging information to FILE." + NL +
            "  -v,--verbose          Verbose output in log file. This can be specified" + NL +
            "                        multiple times, but anything beyond two is neglected." + NL +
            "                        This option is useless without writing to a log file.";
}
