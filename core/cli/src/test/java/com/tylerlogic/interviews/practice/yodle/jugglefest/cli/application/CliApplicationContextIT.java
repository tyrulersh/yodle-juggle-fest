package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application;

import javax.annotation.Resource;

import com.tylerlogic.interviews.practice.yodle.jugglefest.cli.JuggleFestProcessor;
import com.tylerlogic.interviews.practice.yodle.jugglefest.cli.impl.JuggleFestProcessorImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CliApplicationContext.class)
public class CliApplicationContextIT {
    @Resource
    private JuggleFestProcessor juggleFestProcessor;

    @Test
    public void juggleFestProcessorExists() {
        assertThat(juggleFestProcessor, is(notNullValue()));
        assertThat(juggleFestProcessor, is(instanceOf(JuggleFestProcessorImpl.class)));
    }
}
