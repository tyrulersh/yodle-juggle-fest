package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.impl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import com.tylerlogic.interviews.practice.yodle.jugglefest.cli.JuggleFestProcessor;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.JuggleFest;
import com.tylerlogic.interviews.practice.yodle.jugglefest.model.domain.Roster;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.parsing.JuggleFestParser;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.printing.RosterPrinter;
import com.tylerlogic.interviews.practice.yodle.jugglefest.service.roster.RosterService;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.apache.commons.io.IOUtils.toInputStream;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class JuggleFestProcessorTest {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();

    @Mock
    private JuggleFestParser juggleFestParser;
    @Mock
    private RosterService rosterService;
    @Mock
    private RosterPrinter rosterPrinter;
    private JuggleFestProcessor processor;

    @Before
    public void setupCut() {
        processor = new JuggleFestProcessorImpl(juggleFestParser, rosterService, rosterPrinter);
    }

    @Test
    public void solve() {
        JuggleFest juggleFest = new JuggleFest(null, null);
        Roster roster = new Roster(null);
        String printedRoster = "this is a placeholder";
        context.checking(new Expectations() {
            {
                oneOf(juggleFestParser).parse(with(any(Scanner.class)));
                will(returnValue(juggleFest));
                oneOf(rosterService).assignJugglersToCircuits(juggleFest);
                will(returnValue(roster));
                oneOf(rosterPrinter).print(roster);
                will(returnValue(printedRoster));
            }
        });
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        processor.solve(toInputStream("this is the input placeholder"), new PrintStream(output));
        assertThat(output.toString(), is(equalTo(printedRoster)));
    }
}
