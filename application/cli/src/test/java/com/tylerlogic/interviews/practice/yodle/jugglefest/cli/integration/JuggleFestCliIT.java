package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.EXPECTED_HELP_MESSAGE;
import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.NL;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class JuggleFestCliIT extends JuggleFestJarExecutionIT {
    @Test
    public void printHelpUsingShortOption() {
        ExecutionResultContext resultContext = executeJugglefest(null, "-h");
        assertThat(resultContext.getStdoutText(), is(equalTo(EXPECTED_HELP_MESSAGE + NL)));
        assertThat(resultContext.getResultCode(), is(0));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
    }

    @Test
    public void printHelpUsingLongOption() {
        ExecutionResultContext resultContext = executeJugglefest(null, "--help");
        assertThat(resultContext.getStdoutText(), is(equalTo(EXPECTED_HELP_MESSAGE + NL)));
        assertThat(resultContext.getResultCode(), is(0));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
    }

    @Test
    public void jugglersAssignedToTheirPreferredCircuitsReadingFromStdin() {
        String[] inputLines = new String[] {
                "C C1 H:1 E:1 P:1",
                "C C2 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C2",
                "J J2 H:1 E:1 P:1 C1"
        };
        String input = StringUtils.join(inputLines, NL);
        ExecutionResultContext resultContext = executeJugglefest(input);
        assertThat(resultContext.getStdoutText(), is(equalTo(
                "C1 J2 C1:3" + NL +
                "C2 J1 C2:3" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }

    @Test
    public void jugglersAssignedToThePreferredCircuitsTheyMatchBest() {
        String[] inputLines = new String[] {
                "C C1 H:1 E:1 P:1",
                "C C2 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C1",
                "J J2 H:2 E:2 P:2 C1",
                "J J3 H:3 E:3 P:3 C1",
                "J J4 H:4 E:4 P:4 C1"
        };
        String input = StringUtils.join(inputLines, NL);
        ExecutionResultContext resultContext = executeJugglefest(input);
        assertThat(resultContext.getStdoutText(), is(equalTo(
                "C1 J3 C1:9, J4 C1:12" + NL +
                "C2 J1 C1:3, J2 C1:6" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }

    @Test
    public void circuitsAreDisplayedInTheOrderTheyAppearInInput() {
        String[] inputLines = new String[] {
                "C C1 H:1 E:1 P:1",
                "C C2 H:1 E:1 P:1",
                "C C3 H:1 E:1 P:1",
                "C C4 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C1",
                "J J2 H:1 E:1 P:1 C2",
                "J J3 H:1 E:1 P:1 C3",
                "J J4 H:1 E:1 P:1 C4"
        };
        String input = StringUtils.join(inputLines, NL);
        ExecutionResultContext resultContext = executeJugglefest(input);
        assertThat(resultContext.getStdoutText(), is(equalTo(
                "C1 J1 C1:3" + NL +
                "C2 J2 C2:3" + NL +
                "C3 J3 C3:3" + NL +
                "C4 J4 C4:3" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));

        inputLines = new String[] {
                "C C4 H:1 E:1 P:1",
                "C C3 H:1 E:1 P:1",
                "C C2 H:1 E:1 P:1",
                "C C1 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C1",
                "J J2 H:1 E:1 P:1 C2",
                "J J3 H:1 E:1 P:1 C3",
                "J J4 H:1 E:1 P:1 C4"
        };
        input = StringUtils.join(inputLines, NL);
        resultContext = executeJugglefest(input);
        assertThat(resultContext.getStdoutText(), is(equalTo(
                "C4 J4 C4:3" + NL +
                "C3 J3 C3:3" + NL +
                "C2 J2 C2:3" + NL +
                "C1 J1 C1:3" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }

    @Test
    public void juggleFestHandlesJugglesWithSameMatchIndex() {
        // both J0 and J2 should be assigned to C0 even though they have the same match index for it
        String[] inputLines = new String[] {
                "C C0 H:1 E:1 P:1",
                "C C1 H:1 E:1 P:1",
                "J J0 H:5 E:5 P:5 C0",
                "J J1 H:4 E:4 P:4 C0",
                "J J2 H:5 E:5 P:5 C0",
                "J J3 H:3 E:3 P:3 C0"
        };
        String input = StringUtils.join(inputLines, NL);
        ExecutionResultContext resultContext = executeJugglefest(input);
        assertThat(resultContext.getStdoutText(), is(equalTo(
                "C0 J0 C0:15, J2 C0:15" + NL +
                "C1 J3 C0:9, J1 C0:12" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }

    @Test
    public void sampleJuggleFest() {
        String[] inputLines = new String[] {
                "C C0 H:7 E:7 P:10",
                "C C1 H:2 E:1 P:1",
                "C C2 H:7 E:6 P:4",
                "J J0 H:3 E:9 P:2 C2,C0,C1",
                "J J1 H:4 E:3 P:7 C0,C2,C1",
                "J J2 H:4 E:0 P:10 C0,C2,C1",
                "J J3 H:10 E:3 P:8 C2,C0,C1",
                "J J4 H:6 E:10 P:1 C0,C2,C1",
                "J J5 H:6 E:7 P:7 C0,C2,C1",
                "J J6 H:8 E:6 P:9 C2,C1,C0",
                "J J7 H:7 E:1 P:5 C2,C1,C0",
                "J J8 H:8 E:2 P:3 C1,C0,C2",
                "J J9 H:10 E:2 P:1 C1,C2,C0",
                "J J10 H:6 E:4 P:5 C0,C2,C1",
                "J J11 H:8 E:4 P:7 C0,C1,C2"
        };
        String input = StringUtils.join(inputLines, NL);
        ExecutionResultContext resultContext = executeJugglefest(input);
        assertThat(resultContext.getStdoutText(), is(equalTo(
                "C0 J4 C0:122 C2:106 C1:23, J2 C0:128 C2:68 C1:18, J11 C0:154 C1:27 C2:108, " +
                        "J5 C0:161 C2:112 C1:26" + NL +
                "C1 J1 C0:119 C2:74 C1:18, J7 C2:75 C1:20 C0:106, J8 C1:21 C0:100 C2:80, " +
                    "J9 C1:23 C2:86 C0:94" + NL +
                "C2 J0 C2:83 C0:104 C1:17, J10 C0:120 C2:86 C1:21, J3 C2:120 C0:171 C1:31, " +
                    "J6 C2:128 C1:31 C0:188" + NL
                )));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }
}
