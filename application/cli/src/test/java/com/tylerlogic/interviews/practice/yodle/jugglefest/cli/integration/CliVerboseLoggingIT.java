package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

import java.io.File;
import java.io.IOException;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.NL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

/**
 * These tests are technically frail because it relies on, for example, INFO level logging always
 * having less output than DEBUG level logging, and on top of that, relies on logging of code not
 * under control of the Juggle Fest code. However, the likelihood of INFO being less than or equal
 * to DEBUG should be small, especially with logging in the Juggle Fest application. So until it
 * becomes a real problem, the correct operation of the verbose option will be determined by the
 * amount of output in the log files.
 */
public class CliVerboseLoggingIT extends JuggleFestJarExecutionIT {
    private static final String INPUT;

    static {
        String[] inputLines = new String[] {
                "C C1 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C1"
        };
        INPUT = StringUtils.join(inputLines, NL);
    }

    private File logFileOne;
    private File logFileTwo;

    @Before
    public void createTempLogFiles() throws IOException {
        logFileOne = File.createTempFile(CliLoggingIT.class.getSimpleName(), ".log.tmp");
        logFileTwo = File.createTempFile(CliLoggingIT.class.getSimpleName(), ".log.tmp");
    }

    @After
    public void removeTempLogFiles() throws IOException {
        FileUtils.forceDelete(logFileOne);
        FileUtils.forceDelete(logFileTwo);
    }

    @Test
    public void defaultVerbosityIsLessThanSingleVerbosity() {
        assertThat("Test log file '" + logFileOne.getPath() + "' isn't empty. Testing invalid.",
                logFileOne.length(), is(0L));
        assertThat("Test log file '" + logFileTwo.getPath() + "' isn't empty. Testing invalid.",
                logFileTwo.length(), is(0L));
        executeJugglefest(INPUT, "--output=" + logFileOne.getAbsolutePath());
        executeJugglefest(INPUT, "-v", "--output=" + logFileTwo.getAbsolutePath());
        assertThat(logFileOne.length(), is(lessThan(logFileTwo.length())));
    }

    @Test
    public void defaultVerbosityIsLessThanSingleVerbosityUsingLongVerboseOption() {
        assertThat("Test log file '" + logFileOne.getPath() + "' isn't empty. Testing invalid.",
                logFileOne.length(), is(0L));
        assertThat("Test log file '" + logFileTwo.getPath() + "' isn't empty. Testing invalid.",
                logFileTwo.length(), is(0L));
        executeJugglefest(INPUT, "--output=" + logFileOne.getAbsolutePath());
        executeJugglefest(INPUT, "--verbose", "--output=" + logFileTwo.getAbsolutePath());
        assertThat(logFileOne.length(), is(lessThan(logFileTwo.length())));
    }

    @Test
    public void singleVerbosityIsLessThanDoubleVerbosity() {
        assertThat("Test log file '" + logFileOne.getPath() + "' isn't empty. Testing invalid.",
                logFileOne.length(), is(0L));
        assertThat("Test log file '" + logFileTwo.getPath() + "' isn't empty. Testing invalid.",
                logFileTwo.length(), is(0L));
        executeJugglefest(INPUT, "-v", "--output=" + logFileOne.getAbsolutePath());
        executeJugglefest(INPUT, "-vv", "--output=" + logFileTwo.getAbsolutePath());
        assertThat(logFileOne.length(), is(lessThan(logFileTwo.length())));
    }

    @Test
    public void singleVerbosityIsLessThanDoubleVerbosityUsingLongVerboseOption() {
        assertThat("Test log file '" + logFileOne.getPath() + "' isn't empty. Testing invalid.",
                logFileOne.length(), is(0L));
        assertThat("Test log file '" + logFileTwo.getPath() + "' isn't empty. Testing invalid.",
                logFileTwo.length(), is(0L));
        executeJugglefest(INPUT, "--verbose", "--output=" + logFileOne.getAbsolutePath());
        executeJugglefest(INPUT, "--verbose", "--verbose",
                "--output=" + logFileTwo.getAbsolutePath());
        assertThat(logFileOne.length(), is(lessThan(logFileTwo.length())));
    }

    @Test
    public void tripleVerbosityIsSameAsDoubleVerbosity() {
        assertThat("Test log file '" + logFileOne.getPath() + "' isn't empty. Testing invalid.",
                logFileOne.length(), is(0L));
        assertThat("Test log file '" + logFileTwo.getPath() + "' isn't empty. Testing invalid.",
                logFileTwo.length(), is(0L));
        executeJugglefest(INPUT, "-vv", "--output=" + logFileOne.getAbsolutePath());
        executeJugglefest(INPUT, "-vvv", "--output=" + logFileTwo.getAbsolutePath());
        assertThat(logFileOne.length(), is(equalTo(logFileTwo.length())));
    }

    @Test
    public void tripleVerbosityIsSameAsDoubleVerbosityUsingLongVerboseOption() {
        assertThat("Test log file '" + logFileOne.getPath() + "' isn't empty. Testing invalid.",
                logFileOne.length(), is(0L));
        assertThat("Test log file '" + logFileTwo.getPath() + "' isn't empty. Testing invalid.",
                logFileTwo.length(), is(0L));
        executeJugglefest(INPUT, "--verbose", "--verbose",
                "--output=" + logFileOne.getAbsolutePath());
        executeJugglefest(INPUT, "--verbose", "--verbose", "--verbose",
                "--output=" + logFileTwo.getAbsolutePath());
        assertThat(logFileOne.length(), is(equalTo(logFileTwo.length())));
    }
}
