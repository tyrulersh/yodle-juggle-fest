package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.NL;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class InputFileIT extends JuggleFestJarExecutionIT {
    @Test
    public void readingFromAnInputFile() {
        String inputFilePath = JuggleFestCliIT.class.getResource(
                "/InputFileIT/reading-from-an-input-file-test-input.txt").getPath();
        ExecutionResultContext resultContext = executeJugglefest(null, inputFilePath);
        assertThat(resultContext.getStdoutText(), is(equalTo(
                "C1 J2 C1:3" + NL +
                "C2 J1 C2:3" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }

    @Test
    public void noneExistentFile() {
        String filename = FilenameUtils.normalize("/none/existent/file");
        ExecutionResultContext resultContext = executeJugglefest(null, filename);
        assertThat(resultContext.getStdoutText(), is(equalTo("")));
        assertThat(resultContext.getStderrText(), is(equalTo(
                "jf: No such file: " + filename + NL)));
        assertThat(resultContext.getResultCode(), is(1));
    }
}
