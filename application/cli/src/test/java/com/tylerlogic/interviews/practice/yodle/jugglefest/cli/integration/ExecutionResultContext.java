package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

public class ExecutionResultContext {
    private final String stdoutText;
    private final String stderrText;
    private final int resultCode;

    public ExecutionResultContext(String stdoutText, String stderrText, int resultCode) {
        this.stdoutText = stdoutText;
        this.stderrText = stderrText;
        this.resultCode = resultCode;
    }

    public String getStdoutText() {
        return stdoutText;
    }

    public String getStderrText() {
        return stderrText;
    }

    public int getResultCode() {
        return resultCode;
    }
}
