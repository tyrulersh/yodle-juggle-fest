package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

import java.util.Scanner;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class LargeJuggleFestCorrectnessIT extends JuggleFestJarExecutionIT {
    private static final String DESIRED_CIRCUIT_NAME = "C1970";

    @Test
    public void correctNessOfApplicationUsingLargeJuggleFestProblem() {
        String inputFilePath = LargeJuggleFestCorrectnessIT.class.getResource(
                "/large-juggle-fest-description.txt").getPath();
        ExecutionResultContext resultContext = executeJugglefest(null, inputFilePath);
        String desiredCircuitLine = getDesiredCircuitLine(resultContext);
        int sumOfJugglerNames = sumJugglerNamesInLine(desiredCircuitLine);
        assertThat(sumOfJugglerNames, is(28762));
    }

    private String getDesiredCircuitLine(ExecutionResultContext resultContext) {
        String lineOfDesiredCircuit = "";
        Scanner stdoutScanner = new Scanner(resultContext.getStdoutText());
        while (stdoutScanner.hasNextLine()) {
            String line = stdoutScanner.nextLine();
            if (line.startsWith(DESIRED_CIRCUIT_NAME + " ")) {
                lineOfDesiredCircuit = line;
                break;
            }
        }
        stdoutScanner.close();
        return lineOfDesiredCircuit;
    }

    private int sumJugglerNamesInLine(String lineOfDesiredCircuit) {
        int sumOfJugglerNames = 0;
        Scanner circuitLineScanner = new Scanner(lineOfDesiredCircuit);
        while (circuitLineScanner.hasNext()) {
            String token = circuitLineScanner.next();
            if (token.startsWith("J")) {
                sumOfJugglerNames += Integer.parseInt(token.substring(1));
            }
        }
        circuitLineScanner.close();
        return sumOfJugglerNames;
    }
}
