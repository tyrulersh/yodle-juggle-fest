package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

import java.io.File;
import java.io.IOException;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.NL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CliLoggingIT extends JuggleFestJarExecutionIT {
    private static final String INPUT;

    static {
        String[] inputLines = new String[] {
                "C C1 H:1 E:1 P:1",
                "J J1 H:1 E:1 P:1 C1"
        };
        INPUT = StringUtils.join(inputLines, NL);
    }

    private File logFile;

    @Before
    public void createTempLogFile() throws IOException {
        logFile = File.createTempFile(CliLoggingIT.class.getSimpleName(), ".log.tmp");
    }

    @After
    public void removeTempLogFile() throws IOException {
        FileUtils.forceDelete(logFile);
    }

    @Test
    public void simpleExecutionWithVerboseOutputUsingShortOption() {
        assertThat("Test log file isn't empty. Testing invalid.", logFile.length(), is(0L));
        ExecutionResultContext resultContext =
                executeJugglefest(INPUT, "-o", logFile.getAbsolutePath());
        assertThat(logFile.length(), is(greaterThan(0L)));
        assertThat(resultContext.getStdoutText(), is(equalTo("C1 J1 C1:3" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }

    @Test
    public void simpleExecutionWithVerboseOutputUsingLongOptionNoEquals() {
        assertThat("Test log file isn't empty. Testing invalid.", logFile.length(), is(0L));
        ExecutionResultContext resultContext =
                executeJugglefest(INPUT, "--output", logFile.getAbsolutePath());
        assertThat(logFile.length(), is(greaterThan(0L)));
        assertThat(resultContext.getStdoutText(), is(equalTo("C1 J1 C1:3" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }

    @Test
    public void simpleExecutionWithVerboseOutputUsingLongOptionWithEquals() {
        assertThat("Test log file isn't empty. Testing invalid.", logFile.length(), is(0L));
        ExecutionResultContext resultContext =
                executeJugglefest(INPUT, "--output=" + logFile.getAbsolutePath());
        assertThat(logFile.length(), is(greaterThan(0L)));
        assertThat(resultContext.getStdoutText(), is(equalTo("C1 J1 C1:3" + NL)));
        assertThat(resultContext.getStderrText(), is(equalTo("")));
        assertThat(resultContext.getResultCode(), is(0));
    }
}
