package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

import static com.tylerlogic.interviews.practice.yodle.jugglefest.cli.application.IntegrationTestConstants.NL;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ErrorHandlingIT extends JuggleFestJarExecutionIT {
    @Test
    public void usingNonexistentOptionResultsInError() {
        ExecutionResultContext resultContext = executeJugglefest("", "--nonexistent");
        assertThat(resultContext.getStdoutText(), is(equalTo("")));
        assertThat(resultContext.getStderrText(), is(equalTo(
                "jf: Unrecognized option: --nonexistent" + NL +
                "Try 'jf --help' for more information." + NL)));
        assertThat(resultContext.getResultCode(), is(1));
    }

    @Test
    public void passingMoreThanOneArgumentResultsInError() {
        ExecutionResultContext resultContext = executeJugglefest("", "arg1", "arg2");
        assertThat(resultContext.getStdoutText(), is(equalTo("")));
        assertThat(resultContext.getStderrText(), is(equalTo(
                "jf: Program takes one argument at most, instead got: arg1 arg2" + NL +
                "Try 'jf --help' for more information." + NL)));
        assertThat(resultContext.getResultCode(), is(2));

        resultContext = executeJugglefest("", "arg1", "arg2", "arg3");
        assertThat(resultContext.getStdoutText(), is(equalTo("")));
        assertThat(resultContext.getStderrText(), is(equalTo(
                "jf: Program takes one argument at most, instead got: arg1 arg2 arg3" + NL +
                "Try 'jf --help' for more information." + NL)));
        assertThat(resultContext.getResultCode(), is(3));
    }

    @Test
    public void nonexistentPreferredJugglerResultsInError() {
        String input =
                "C circuitName H:1 E:1 P:1" + NL +
                "J jugglerName H:1 E:1 P:1 nonexistentCircuit";
        ExecutionResultContext resultContext = executeJugglefest(input);
        assertThat(resultContext.getStdoutText(), is(equalTo("")));
        assertThat(resultContext.getStderrText(), is(equalTo(
                "jf: Juggler 'jugglerName' prefers nonexistent circuit: 'nonexistentCircuit'" +
                        NL)));
        assertThat(resultContext.getResultCode(), is(1));
    }
}
