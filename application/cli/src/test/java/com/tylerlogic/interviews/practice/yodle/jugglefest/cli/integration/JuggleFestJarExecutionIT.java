package com.tylerlogic.interviews.practice.yodle.jugglefest.cli.integration;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.junit.BeforeClass;

public abstract class JuggleFestJarExecutionIT {
    private static final String EXTRA_JVM_PARAMETERS;

    static {
        // this rigmarole allows for system properties to be specified within the value of the
        // "jugglefest.jar.execution.test.jvm.parameters" system property
        String extraJvmParameters =
                System.getProperty("jugglefest.jar.execution.test.jvm.parameters");
        extraJvmParameters = StrSubstitutor.replaceSystemProperties(extraJvmParameters);
        EXTRA_JVM_PARAMETERS = StringUtils.strip(extraJvmParameters, "'\"");
    }

    private static final String JAVA_EXECUTABLE_PATH =
            FilenameUtils.normalize(System.getProperty("jugglefest.java.home") + "/bin/java");
    private static final String JUGGLE_FEST_JAR_FILE = System.getProperty("jugglefest.jar.path");

    @BeforeClass
    public static void ensureJuggleFestJarExists() {
        if (!new File(JUGGLE_FEST_JAR_FILE).exists()) {
            throw new RuntimeException(
                    "Non-existent JuggleFest executable jar: " + JUGGLE_FEST_JAR_FILE);
        }
    }

    protected ExecutionResultContext executeJugglefest(String input, String... args) {
        List<String> command = buildCommand(args);
        ExecutionResultContext resultContext;
        try {
            Process process = new ProcessBuilder().command(command).start();
            OutputStream stdin = process.getOutputStream();
            stdin.write(StringUtils.defaultString(input).getBytes());
            stdin.close();
            String stdoutText = IOUtils.toString(process.getInputStream());
            String stderrText = IOUtils.toString(process.getErrorStream());
            waitForProcess(process);
            int resultCode = process.exitValue();
            resultContext = new ExecutionResultContext(stdoutText, stderrText, resultCode);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
        return resultContext;
    }

    private List<String> buildCommand(String[] args) {
        List<String> command = newArrayList(JAVA_EXECUTABLE_PATH, "-jar", JUGGLE_FEST_JAR_FILE);
        if (StringUtils.isNotBlank(EXTRA_JVM_PARAMETERS)) {
            command.add(1, EXTRA_JVM_PARAMETERS);
        }
        command.addAll(toList(args));
        System.out.println("Built command: " + command);
        return command;
    }

    private void waitForProcess(Process process) {
        try {
            process.waitFor();
        } catch (InterruptedException interruptedException) {
            throw new RuntimeException(interruptedException);
        }
    }

    private List<String> toList(String[] values) {
        ArrayList<String> list = new ArrayList<>();
        for (String s : values) {
            list.add(s);
        }
        return list;
    }
}
